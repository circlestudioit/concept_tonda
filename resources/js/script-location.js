Environment = {
    //mobile or desktop compatible event name, to be used with '.on' function
    TOUCH_DOWN_EVENT_NAME: 'mousedown touchstart',
    TOUCH_UP_EVENT_NAME: 'mouseup touchend',
    TOUCH_MOVE_EVENT_NAME: 'mousemove touchmove',
    TOUCH_DOUBLE_TAB_EVENT_NAME: 'dblclick dbltap',

    isAndroid: function() {
        return navigator.userAgent.match(/Android/i);
    },
    isAndroidTablet: function() {
        return navigator.userAgent.match(/Android 3.0/i);
    },
    isBlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    isIOSTablet: function() {
        return navigator.userAgent.match(/iPad/i);
    },
    isIOSPhone: function() {
        return navigator.userAgent.match(/iPhone|iPod/i);
    },       
    isOpera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    isWindows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    isChromeMobile: function() {
        return navigator.userAgent.match(/Chrome/[.0-9]* /Mobile/i);
    },
//    isChromeTablet: function() {
//       return navigator.userAgent.match('Chrome/[.0-9]* (?!Mobile)'); 
//    },
    
    isMobile: function() {
        return (Environment.isAndroid() || Environment.isBlackBerry() || Environment.isIOSPhone() || 
                Environment.isOpera() || Environment.isWindows() || Environment.isChromeMobile());
    },
    isTablet: function() {
        return (Environment.isIOSTablet() || Environment.isAndroidTablet());
    }
};

if(Environment.isMobile()) { //MOBILE
    $(window).load(function() {
        //$('ul.slides').height($(window).height());
    });
}

else if(Environment.isTablet()) { //TABLET
    $(window).load(function() {
        //$('ul.slides').height($('.content-interne-azienda').outerHeight());
    });
    
}

else { //DESKTOP
    
    $(window).load(function() {
        //$('ul.slides').height($('.content-interne-azienda').outerHeight());
    });
    
}

/* Funzioni chiamate su tutti i dispositivi */

$('.flexslider').flexslider({
    animation: "fade",
    randomize: true,
    controlNav: false,
    directionNav: false
});

if(document.getElementById('promo-slider')) {
    $('#promo-slider').flexslider({
        animation: "slide",
        randomize: false,
        controlNav: true,
        directionNav: false,
        slideshow: false
    });
}

if(document.getElementById('staff-slider')) {
    $('#staff-slider').flexslider({
        animation: "slide",
        randomize: false,
        controlNav: true,
        directionNav: false,
        slideshow: false
    });
}

  
  $(window).resize(function() {
//      $('ul.slides').height($('.content-interne-azienda').outerHeight());
  });
  
  
$('#submit').click(function() {
    $('#form-messages').fadeIn();
    var form_data = {
        first_name: $('#first-name').val(),
        last_name: $('#last-name').val(),
        country: $('#country').val(),
        email: $('#email').val(),
        richieste: $('#richieste').val()
    };
    $.ajax({
        url: "/ospitalita-request",
        type: 'POST',
        data: form_data,
        success: function(msg) {
               
            $('#form-messages').html(msg);
            setTimeout(function() {
                $('#form-messages').fadeOut().html();
                
            }, 3000);
        }
    });
    return false;
});