$(window).load(function () {
    
    $('.flexslider').flexslider({
        directionNav : false
    });    
    
    $('.roomslider').flexslider({
        direction : 'vertical',
        slideshow: false,
        animation: 'slide',
        animationLoop: true,
        customDirectionNav: '.dirnav',
        manualControls: '.customdir'
    });
    
});

$(document).ready(function( ){
    $('.pagina-interna-content').css('margin-top', $(window).height()+'px');
    $('.fullscreenslider, .halfscreenslider').height($(window).height());
    
    $('.room-preview').on('mouseenter touchstart',function() {
        event.stopPropagation();
        $(this).children('.opacity-layer-room').transit({'display' : 'table-column'}, 200);
        $(this).children('a.room-name').transit({'display' : 'table-cell'});
    }).on('mouseleave',function() {
        event.stopPropagation();
        $(this).children('.opacity-layer-room').transit({'display' : 'none'}, 200);
        $(this).children('a.room-name').transit({'display' : 'none'});
    });
    
    
    
    
});