$(window).load(function() {
     
    $('.container-featured-gallery, .featured-gallery-fixed').height($(window).outerHeight());
     
    $('.flexslider').flexslider({
        animation: "fade",
        randomize: false,
        controlNav: true,
        directionNav: true
    });
  });
  
  $(window).resize(function() {
    $('.container-featured-gallery, .featured-gallery-fixed').height($(window).outerHeight());
  });