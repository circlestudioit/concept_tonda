function googleMaps() {
    // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 15,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(42.4789151,14.1941337), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"on"},{"color":"#716464"},{"weight":"0.01"}]},{"featureType":"administrative.country","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"},{"color":"#a05519"},{"saturation":"-13"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#fff100"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"simplified"},{"color":"#84afa3"},{"lightness":52}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"visibility":"on"}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(42.4789151,14.1941337),
                    map: map,
                    title: 'Circle Studio srl'
                });
            }
}

function initSquares() {   
    $('.square').height($('.square').width());
}


function playVideo() {
    $('#video').fadeIn();

    var options = {
        autoplay: true,
        loop: false
    };
    
    iframe = document.querySelector('iframe');
    player = new Vimeo.Player(iframe, options);
    player.setVolume(0);

    player.play();
    
    invisible = false;
    
    $(".sidedock,.controls").remove();
    
    player.on('ended', function() {
        invisible = true;
        $('#video').fadeOut(function() {
            $(".poster").fadeIn(500);
            $("#titlepage").fadeIn(1000);
            $(".scroll").fadeIn(1500);
        });
        
        
    });
    
}

function initSquare(className) {
    
    w_square = $('.w_square').width();
	$('.w_square').css('height', w_square+'px');
    $('.'+ className +'').css('height', w_square+'px');
    

}

function openLangMenu()
{
   $('.list-lang').fadeIn().slideDown();
//   setTimeout(function() {
//       $('.list-lang').fadeOut();
//   }, 3000);
}

function resizeVideo(vwidth, id) {
    var w = vwidth;
    var h = w*1080/1920;
    $('#video').css({'width' : w, 'height' : h});
    $('#video video').attr({'width' : w, 'height' : h});
    
    $('#video iframe').attr("width", w);
    $('#video iframe').attr("height", h);
    
    $('#videoslide iframe').attr("width", w);
    $('#videoslide iframe').attr("height", h);
}

function setVideoSize(width) {
    
    var w = width;
    var h = w*1080/1920;
    $('#video').css({'width' : w, 'height' : h});
    //, 'margin-top' : $('.navbar').innerHeight()+'px'
    $('#video video').attr({'width' : w, 'height' : h});
    
    $('#videoslide iframe').attr("width", w);
    $('#videoslide iframe').attr("height", h);

}

function resizeVideo_intro(vwidth) {
    var w = vwidth;
    var h = w*1080/1920;
    $('#video-intro').css({'width' : w, 'height' : h});
    $('#video-intro video').attr({'width' : w, 'height' : h});
    
    $('#video-intro iframe').attr("width", w);
    $('#video-intro iframe').attr("height", h);
    
    $('#video-intro iframe').attr("width", w);
    $('#video-intro iframe').attr("height", h);
}

function setVideoSize_intro(width) {
    
    var w = width;
    var h = w*1080/1920;
    $('#video-intro').css({'width' : w, 'height' : h});
    //, 'margin-top' : $('.navbar').innerHeight()+'px'
    $('#video-intro video').attr({'width' : w, 'height' : h});
    
    $('#video-intro iframe').attr("width", w);
    $('#video-intro iframe').attr("height", h);

}

function send_mail() {
    
      $.ajax({
        type: 'POST',
        url: document.location.origin+'/home/send_mail', /* pageurl */
        success: function(data) {
            
            /* Carico il contenuto del lavoro nel div */
            //var pdata = $.parseJSON(data);
            $('#form-success').html("Grazie per aver inviato la tua richiesta.");
            
        },
        error: function(data) {
            console.log('ajax error: '+data);
        }
    });
}

//function openRestaurantsMobile(country)
//{
//    $.ajax({
//        type: 'POST',
//        url: document.location.origin+'/home/get_restaurants_for_country/'+country, /* pageurl */
//        success: function(data) {
//            /* Carico il contenuto del lavoro nel div */
//            var pdata = $.parseJSON(data);
//            //console.log(pdata);
//            $('#restaurants-dropdown').empty();
//            for (i = 0; i < pdata.length; i++) { 
//                
//                if(pdata[i].published == 1 && pdata[i].next_opening == 0) {
//                $('#restaurants-dropdown').append(
//                    $('<li><a href="'+document.location.origin+'/location/'+pdata[i].loc_name+'">'+pdata[i].store_short_name+'<br /><small class="city-name-nav">'+pdata[i].city+'</small></a></li>'));
//                }
//                else if(pdata[i].next_opening == 1) {
//                    $('#restaurants-dropdown').append(
//                    $('<li class="inactive"><a href="'+document.location.origin+'/location/'+pdata[i].loc_name+'">'+pdata[i].store_short_name+'<br /><small class="city-name-nav inactive">'+pdata[i].city+'</small></a></li>'));
//                }
//
//            }
//            //$('#nav-restaurants').slideDown();
//        },
//        error: function(data) {
//            console.log('ajax error: '+data);
//        }
//    });
//    
//    //to change the browser URL to the given link location
////    if(pagelink!=window.location) {
////        window.history.pushState({path:'/'+pagelink},'', '/'+pagelink);
////    }
//    //stop refreshing to the page given in
//    return false;
//    
//}

function closeNavLocations() {
    $('#nav-restaurants').slideUp(500, function() {
//        $('#nav-cities').slideUp(300, function() {
            $('.nav-locations').slideUp(200);
//        });
    });
}

$('.list-lang').mouseleave(function() {
    $('.list-lang').slideUp().fadeOut();
});

function showhomeMenu(menuItem) {
    //var h = $("article[data-id="+menuItem+"]").height();
    $("article[data-id="+menuItem+"] .overlay-menu-home").transit({'margin-top' : '-'+$("article[data-id="+menuItem+"] .overlay-menu-home").height()+'px'}, 500);
    $("article[data-id="+menuItem+"]").transit({'background-size' : '130%'});
    if(menuItem === 'cocconline') {
        $('.cart-tondo').fadeOut();
    }
}

function hidehomeMenu(menuItem) {
    $("article[data-id="+menuItem+"] .overlay-menu-home").transit({'margin-top' : '0px'}, 500);
    $("article[data-id="+menuItem+"]").transit({'background-size' : '110%'});
    if(menuItem === 'cocconline') {
        $('.cart-tondo').fadeIn();
    }
}

function showFixedMenuItems() {
    $('.overlay-menu-home').css('margin-top', '-'+$('.overlay-menu-home').height()+'px');
    $('.overlay-prodotti').css('margin-top', '-'+$('.overlay-prodotti').height()+'px'); 

    $('#f1_container').on('touchstart', function() {
        $('#f1_container').toggleClass('hover_effect');
        if($('#f1_container').hasClass('hover_effect')) {
            $('.front').fadeOut();
        }
        else {
            $('.front').fadeIn();
        }
    });
                    
}

function setupMap(height) {
    $('.map-canvas').height(height);
}

/* Menu Action */
function openMenu() {
    $("#menu").fadeIn();
    $("#menu-icon").addClass('on');
    
}

function closeMenu() {
    $("#menu").slideLeftHide();
}


jQuery.fn.extend({
  slideRightShow: function() {
    return this.each(function() {
        $(this).show('slide', {direction: 'right'}, 500);
    });
  },
  slideLeftHide: function() {
    return this.each(function() {
      $(this).hide('slide', {direction: 'left', easing: 'easeOutExpo'}, 700);
    });
  },
  slideRightHide: function() {
    return this.each(function() {
      $(this).hide('slide', {direction: 'right'}, 500);
    });
  },
  slideLeftShow: function() {
    return this.each(function() {
      $(this).show('slide', {direction: 'left', easing: 'easeOutQuart'}, 1000);
    });
  }
});