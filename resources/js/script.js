$(document).ready(function() {
    $(window).load(function() { // makes sure the whole site is loaded
        setTimeout(function(){
            $("#status").fadeOut(); // will first fade out the loading animation
            $("#preloader").delay(250).fadeOut("slow", function() {
            }); // will fade out the white DIV that covers the website.
        }, 500);
    });
    
    /* Inizializza altezze dei quadrati della home page*/
//    initSquares();

    var s = skrollr.init({
        render: function(data) {
		//Log the current scroll position.
		//console.log(data.curTop);
	}
    });
    
    $('#accoglienza').innerHeight($("#i_am_tonda").height());
    $('#artigiani-poster').innerHeight($("#slider-pizzerie ul li").innerHeight());
    $('#gusto-locale-container').innerHeight($("#londra").height());
    $('#friarielli_caption').innerHeight($("#friarielli").innerHeight());
    $('#salad').innerHeight($("#salad_right").innerHeight());
//    $('#prodottiamarchio').outerHeight($("#prodotti_a_marchio_container").outerHeight());
    
    
    /* promo tonda 2015 */
//    if(document.getElementById('video-intro')) {
//        resizeVideo_intro($(window).width());
//        setVideoSize_intro($(window).width());
//        playVideo();
//    }

    
    /* Ridimensiona il video su smartphone */
    if(document.getElementById('video')) {
        resizeVideo($(window).width()*.8);
        setVideoSize($(window).width()*.8);
    }

    
    
    $('#slider-pizzerie').flexslider({
        directionNav: false,
        easing: 'linear',
        slideshowSpeed: 3500,
        animationSpeed: 1000,
        randomize: true,
        controlNav: true
    });
    
    $('#map').height($('#contactcontent').height());
    

    
    
    $('a[href^="#"]').click(function(e) {
        // Prevent the jump and the #hash from appearing on the address bar
        e.preventDefault();
        // Scroll the window, stop any previous animation, stop on user manual scroll
        // Check https://github.com/flesler/jquery.scrollTo for more customizability
        $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:true});
    });
    
    
    // $('#submit').click(function() {
    //     $('#form-messages').fadeIn();
    //     var form_data = {
    //         nome: $('#nome').val(),
    //         company: $('#company').val(),
    //         role: $('#role').val(),
    //         email: $('#email').val(),
    //         richieste: $('#richieste').val()
    //     };
    //     $.ajax({
    //         url: "/contattaci",
    //         type: 'POST',
    //         data: form_data,
    //         success: function(msg) {

    //             $('#form-messages').html(msg);
    //             setTimeout(function() {
    //                 $('#form-messages').fadeOut().html();

    //             }, 3000);
    //         }
    //     });
    //     return false;
    // });


    $('#submit').click(function() {
        $('#form-messages').fadeIn();
        var password = $('#password').val();

        if (password == 'bollente') {
            $('#form-messages').html(msg);
                setTimeout(function() {
                    $('#form-messages').fadeOut().html();

                }, 3000);
        }
        return false;
    });


    
}); //document.ready
 
$(window).resize(function() {
    
    //$('#coverimage').height($(window).height()*0.75);
    $('#accoglienza').innerHeight($("#i_am_tonda").height());
    $('#artigiani-poster').innerHeight($("#slider-pizzerie ul li").innerHeight());
    $('#gusto-locale-container').innerHeight($("#londra").height());
    $('#friarielli_caption').innerHeight($("#friarielli").innerHeight());
    $('#salad').innerHeight($("#salad_right").innerHeight());
    
    /* Inizializza quadrati home page*/
    initSquares();

    if(document.getElementById('video')) {
        resizeVideo($(window).width()*.8);
        setVideoSize($(window).width()*.8);
    }
    
    $('#beginpage').css('margin-top', $(window).height());

});

//$(window).bind("scroll", function(e) {
//
////    if($(window).scrollTop() > 0) {
////        $('.scroll').fadeOut();
////
//////        $('.logo-home').fadeOut();
////    }
////    else {
////        $('.scroll').fadeIn();
////        
////    }
//    
//    /* START PARALLAX */
////    if ($(window).scrollTop() < window.innerHeight)
////    {
////            var percent = Math.min(1, $(window).scrollTop() / window.innerHeight);
////            $(".page-cover").css("top", (percent * -5)+"%");
////            $(".page-cover-top").css("top", (percent * -15)+"%");
////            $(".page-image-top").css("top", (percent * -15)+"%");
////    }
//    /* /START PARALLAX */
//    
////    if(document.getElementById('video')) {
////        if ($(window).scrollTop() > window.innerHeight /2) {
////                player.pause();
////        }
////        else {
////            if(!invisible)
////                player.play();
////        }
////    }
//});