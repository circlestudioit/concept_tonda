$(document).ready(function() {
    $(window).load(function() { // makes sure the whole site is loaded
        setTimeout(function(){
            $("#status").fadeOut(); // will first fade out the loading animation
            $("#preloader").delay(250).fadeOut("slow", function() {
                
            }); // will fade out the white DIV that covers the website.
        }, 500);
    });
   
    
    $('#accoglienza').innerHeight($("#i_am_tonda").height());
    $('#artigiani-poster').innerHeight($("#slider-pizzerie ul li").innerHeight());
//    $('#gusto-locale-container').innerHeight($("#londra").height());
//    $('#friarielli_caption').innerHeight($("#friarielli").innerHeight());
    $('#salad').innerHeight($("#salad_right").innerHeight());
    
    
    /* Ridimensiona il video su smartphone */
    if(document.getElementById('video')) {
        resizeVideo();
        setVideoSize();
//        playVideo();
    }
    
    $('#beginpage').css('margin-top', $(window).height());
    
    $('#coverimage').height($(window).height()*0.75);
    
//    $('#slider-hp').height($(window).height());
    
    $('#slider-pizzerie').flexslider({
        directionNav: false,
        easing: 'linear',
        slideshowSpeed: 3500,
        animationSpeed: 1000,
        randomize: true,
        controlNav: true
    });
    
    $('#map').height($('#contactcontent').height());
    
    
    $("a.ajax-link").click(function(e) {
        //prevent default action
        e.preventDefault();

        //define the target and get content then load it to container
        var l = $(this).attr("href");
        link = l.split("/");
        baselink = link[0];
        pagelink = link[1];
        //console.log(baselink+'/'+pagelink);
        openSubPage(baselink, pagelink);
    });

    
//    $('.openmenu').click(function() {
//        if($('#menu-icon').hasClass('on')){
//            $('#menu-icon').removeClass('on');
//            $('nav#nav-interna').fadeOut();
//            $('body, html').css('overflow','visible');
//        }
//        else{
//            $('nav#nav-interna').fadeIn();
//            $('#menu-icon').addClass('on');           
//            $('body, html').css('overflow','hidden');
//        }
//    });
    
    
    $('a[href^="#"]').click(function(e) {
        // Prevent the jump and the #hash from appearing on the address bar
        e.preventDefault();
        // Scroll the window, stop any previous animation, stop on user manual scroll
        // Check https://github.com/flesler/jquery.scrollTo for more customizability
        $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:true});
    });
    
    
    $('#submit').click(function() {
        $('#form-messages').fadeIn();
        var form_data = {
            nome: $('#nome').val(),
            company: $('#company').val(),
            role: $('#role').val(),
            email: $('#email').val(),
            richieste: $('#richieste').val()
        };
        $.ajax({
            url: "/contattaci",
            type: 'POST',
            data: form_data,
            success: function(msg) {

                $('#form-messages').html(msg);
                setTimeout(function() {
                    $('#form-messages').fadeOut().html();

                }, 3000);
            }
        });
        return false;
    });
    
}); //document.ready
 
$(window).resize(function() {
    
    $('#coverimage').height($(window).height()*0.75);
    
    /* Inizializza quadrati home page*/
    //initSquares();
    
    /* Ridimensiona il video su smartphone */
    if(document.getElementById('video')) {
        resizeVideo();
        setVideoSize();
    }
//    playVideo();
    
    $('#beginpage').css('margin-top', $(window).height());

});

$(window).bind("scroll", function(e) {

    if($(window).scrollTop() > 0) {
        $('.scroll').fadeOut();

//        $('.logo-home').fadeOut();
    }
    else {
        $('.scroll').fadeIn();
        
    }
    
    /* START PARALLAX */
    if ($(window).scrollTop() < window.innerHeight)
    {
            var percent = Math.min(1, $(window).scrollTop() / window.innerHeight);
            $(".page-cover").css("top", (percent * -5)+"%");
            $(".page-cover-top").css("top", (percent * -15)+"%");
            $(".page-image-top").css("top", (percent * -15)+"%");
    }
    /* /START PARALLAX */
    
//    if(document.getElementById('video')) {
//        if ($(window).scrollTop() > window.innerHeight /2) {
//                player.pause();
//        }
//        else {
//            if(!invisible)
//                player.play();
//        }
//    }
});