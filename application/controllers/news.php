<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends MY_Controller {
	
        public function ChangeLanguage($lang){
            $this->session->set_userdata('lang', $lang);
            redirect($this->session->userdata('currentPage'));
        }
        
        
        public function index() {
		
            /*
             *set up title and keywords (if not the default in custom.php config file will be set) 
             */

            $this->title = "Circle Studio";
            $this->keywords = "";
            $this->description = "";
            $this->author = "Circle Studio";
            $this->javascript = array("libs/jquery.transit.min.js", "script.js", "libs/jquery.mixitup.js",
                                      "functions.js", "script-news.js");
            $this->css = array("main.css", "responsive.css", "flexslider.css"); 
            $this->fonts = array("Lato:100,300,400,700");

            if($this->session->userdata('lang') == null)
                $this->session->set_userdata('lang', $this->config->item('language'));
            
            
            $this->_render_template_news();
        }
        
        public function get_news($post_name) {
		
            /*
             *set up title and keywords (if not the default in custom.php config file will be set) 
             */

            $detect = new Mobile_Detect();
            
            $this->title = "Circle Studio";
            $this->keywords = "";
            $this->description = "";
            $this->author = "Circle Studio";
            
            if ($detect->isMobile())
            {
                $this->javascript = array("libs/jquery.mobile-1.4.3.min.js", "libs/jquery.transit.min.js", "libs/jquery.easings.min.js",
                                      "libs/jquery.flexslider-min.js", "functions.js", "script.min.js");
            }
            else {
                $this->javascript = array("libs/jquery.mobile-1.4.3.min.js", "libs/jquery.transit.min.js", "libs/jquery.easings.min.js",
                                      "libs/jquery.flexslider-min.js", "functions.js", "script.min.js");
            }
            
            $this->css = array("main.css", "responsive.css", "flexslider.css", "hoverset1.css"); 
            $this->fonts = array("Lato:100,300,400,700");

            if($this->session->userdata('lang') == null)
                $this->session->set_userdata('lang', $this->config->item('language'));
            
            
            $this->_render_news($post_name);
        }
}