<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
        
        
        public function index() {
		
            /*
             *set up title and keywords (if not the default in custom.php config file will be set) 
             */
            $detect = new Mobile_Detect();
            
            $this->title = "Tonda - Authentic food. Organic pizzetta. Italian taste";
            $this->keywords = "";
            $this->description = "";
            $this->author = "Circle Studio";
            
            if ($detect->isMobile())
            {
                $this->javascript = array("libs/jquery.mobile-1.4.3.min.js", "libs/jquery.transit.min.js", "libs/jquery.easings.min.js",
                                      "libs/jquery.flexslider-min.js", "functions.js", "script_mobile.js");
            }
            else {
                $this->javascript = array("libs/jquery.mobile-1.4.3.min.js", "libs/jquery.transit.min.js", "libs/jquery.easings.min.js", "libs/skrollr.js",
                                      "libs/jquery.flexslider-min.js", "functions.js", "script.js");
            }
            
            $this->css = array("main.css", "responsive.css", "flexslider.css", "hoverset1.css"); 
            $this->fonts = array("Lato:100,300,400,700");

            if($this->session->userdata('lang') == null)
                $this->session->set_userdata('lang', $this->config->item('language'));


           
            $this->_render('home');

        }
        
        
        
        public function send_mail() {
            
            $this->load->library('email');
            $this->load->library('form_validation');
            $this->load->library('upload');
            $this->load->helper(array('form', 'url'));
            
            $this->form_validation->set_rules('nome', 'First Name', 'required');
            $this->form_validation->set_rules('company', 'Company', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
//            $this->form_validation->set_rules('country', 'Country', 'required');
            //$this->form_validation->set_rules('privacy-check', 'Privacy', 'required');
            
            
            if ($this->form_validation->run() == FALSE) {
                $errors = validation_errors();
//                echo json_encode($errors);
                $curpage = str_replace("/index.php", "", $this->session->userdata('currentPage'));
                //$this->session->set_flashdata('errors', $errors);
                return print_r($errors);
                /* L'errore è gestito via ajax in script.min.js */
                
                //redirect($curpage);
            }
            else {
                $config['charset']          = 'iso-8859-1';
                $config['mailtype']         = 'html';
                $this->email->initialize($config);

                $this->email->from($_POST['email'], $_POST['nome']);
                $this->email->to('hello@circlestudio.it'); 
                //$this->email->cc('another@another-example.com'); 
//                $this->email->bcc('r.rotondo@circlestudio.it'); 
                
//                if($_FILES['upload']['size'] > 0) { // upload is the name of the file field in the form
//
//                    $aConfig['upload_path']      = $_SERVER['DOCUMENT_ROOT'].'/upload_area/';
//                    $aConfig['allowed_types']    = 'doc|docx|pdf|jpg|png';
//                    $aConfig['max_size']         = '3000';
//                    $aConfig['max_width']        = '1280';
//                    $aConfig['max_height']       = '1024';
//
//                    $this->upload->initialize($aConfig);
//                    //print_r($aConfig['upload_path']);
//                    if($this->upload->do_upload('upload'))
//                    {
//                        $image_data = $this->upload->data();
//                        $fname=$image_data['file_name'];
//                        $fpath=$image_data['file_path'].$fname;
//
//
//                    } else {
//                        print_r("upload error: ".$this->upload->display_errors());
//                    }
//                }
//                if(isset($fpath) && $fpath != null) {
//                    $this->email->attach($fpath);
//                }

                $this->email->subject('Circle Studio Website Contact');
                $this->email->message(
                        "<!DOCTYPE html>
                        <head>
                        <meta charset=\"utf-8\">
                        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
                        <title>Masciarelli - Ospitalità</title>
                        </head>
                        <body>
                        Nome: ".(isset($_POST['nome'])? $_POST['nome'] : "")."<br />".
                        "Azienda: ".(isset($_POST['company'])? $_POST['company'] : "")."<br />".
                        "Ruolo: ".(isset($_POST['role'])? $_POST['role'] : "")."<br />".
                        "Email: ".(isset($_POST['email'])? $_POST['email'] : "")."<br /><br />".
                        "Messaggio: ".$_POST['richieste']."<br /><br />".
                        "</body></html>"

                );	


                echo $this->email->print_debugger();

                $this->email->send();
                //$curpage = str_replace("/index.php", "", $this->session->userdata('currentPage'));
                //redirect($curpage);
                return print_r($this->lang->line('grazie-per-aver-inviato'));
            }
            
        }
            
        
}