<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';


//$route['frontend/(:any)'] = "index.php/$1";

/* L'ORDINE DI QUESTE ENTRIES CONTA !!! */

$route['news/(:any)']                                   = "news/get_news/$1";
$route['news']                                          = "news";

$route['what-we-do']                                    = "home/cosa_facciamo";
$route['events']                                        = "home/fud";
$route['works']                                         = "home/lavori";
$route['works/(:any)']                                  = "home/work";
$route['contact']                                       = "home/contatti";
$route['awards']                                        = "home/awards";

$route['contattaci']                                    = "home/send_mail";
//$route['home/send_franchising_request_mail']            = "home/send_franchising_request_mail";
$route['lang/(:any)']                                   = "lang/ChangeLanguage/$1";

//$route['favicon.ico']                                   = "home";

$route['(:any)']                                        = "home/paginatestuale";

/* ajax content */
//$route['home/get_senso/(:any)'] = "home/get_senso/$1";
//$route['home/get_subpage_content/(:any)'] = "home/get_subpage_content/$1";
/* ajax content */

//$route['(:any)/(:any)'] = "mainpage/subpage";
//$route['(:any)'] = "home/paginatestuale";



/* End of file routes.php */
/* Location: ./application/config/routes.php */