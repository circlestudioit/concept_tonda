<?php

$lang['header_title']                   = "Authentic food.<br />Organic pizzetta.<br />Italian taste.";
$lang['obiettivo_title']		= "CHALLENGE";
$lang['obiettivo_content']		= "Tonda nasce dalla volontà di trasmettere ad un pubblico internazionale l’eccellenza della ristorazione veloce italiana di qualità, all’interno di uno spazio dal sapore italiano.";

$lang['story_title']                    = "Abruzzo, Italia, anni 60";
$lang['story_caption']                  = "Gabriele Ciferni e sua moglie Vincenzina<br />con i suoi genitori";
$lang['story_caption_right']		= "Gabriele Ciferni e suo padre sulla spiaggia di Pescara";

$lang['soluzione_title']		= "IDEA";
$lang['soluzione_content']		= "Diamo vita ad un format in cui protagonista è un prodotto storico in Italia ma innovativo per il mercato estero: la pizzetta tonda cotta al padellino, nata in Italia dalle mani di Gabriele Ciferni nel 1958.<br />
                                           La volontà di abbracciare un pubblico internazionale eterogeneo rende necessario formulare un’offerta più ampia. Per questo vengono selezionati piatti della ristorazione veloce tipicamente italiani universalmente riconosciuti e apprezzati.";

$lang['format_content']                 = "<br /><strong>Tonda</strong> è un format fast casual che offre cibo salutare di alta qualità, servito in un ambiente di design con un menu dai costi contenuti.<br /><br />";
$lang['authenticorganic']               = "AUTHENTIC FOOD . ORGANIC PIZZETTA . ITALIAN TASTE";

$lang['offerta-chiara-title']           = "Offerta chiara sin da fuori.";
$lang['offerta-chiara-content']         = "Passando davanti alla vetrina deve essere chiara la tipologia di prodotto disponibile. L’attenzione è focalizzata sulle caratteristiche di base dei prodotti e del format (Authentic Organic Italian).<br />
                                           Una vetrofania lascia intuire la varietà della selezione di prodotti presenti all'interno dello store.<br />Laddove possibile sarà utile prevedere dei cavalletti esterni per particolari momenti della giornata o per \"specials del giorno\" e potrebbe essere previsto un supporto fisso con il menu esposto.";

$lang['accoglienza-title']              = "Accoglienza italiana";
$lang['accoglienza-content']            = "Essere italiani significa avere attenzione verso gli altri. Nei locali diamo il benvenuto ai nostri clienti e li facciamo sentire in Italia.";
$lang['accoglienza-post-title']         = "Ciao, welcome to <strong>Tonda</strong>";

$lang['benvenuto-in-italia']            = "Benvenuto in Italia";
$lang['benvenuto-in-italia-content']    = "Gli elementi di allestimento delle vetrine, unitamente all'uso dell'apetta brandizzata ove possibile, renderanno tutto il sapore italiano necessario per attiare i consumatori ad entrare in <strong>Tonda</strong>.";

$lang['the-dubai-store-title']          = "The Dubai Store";
$lang['the-dubai-store-content']        = "Dal food agli arredi un totale richiamo alla tradizione; minimal e vintage si mescolano dando vita ad un'atmosfera casalinga, calda e accogliente, fatta di ricordi e di memorie.";

$lang['offerta-title']                  = "BUONISSIMA!";
$lang['offerta-concept']                = "Pizzetta come prodotto assolutamente unico e particolare.<br />Da raccontare e spiegare per consentire la migliore esperienza possibile.<br />Offerta di altri prodotti italiani a supporto del <em>main product</em>.";
$lang['offerta-varia-title']            = "Un'offerta varia ma mirata.";
$lang['offerta-varia-content']          = "La pizzetta è il prodotto cardine del format e deve essere spiegato di cosa si tratta e come fruire al meglio dell’esperienza in Tonda.
                                           La possibilità di approfondire le informazioni sulle materie prime è lasciata a supporti più specifici.<br />
                                           È consigliabile prevedere l’utilizzo di monitor dietro il banco che trasmettano video e immagini dei prodotti fornendo informazioni sull’offerta.";

$lang['la-nostra-pizzetta']             = "La nostra pizzetta è fatta con <strong>impasto certificato biologico</strong>, con l’uso di <strong>pochissimo lievito naturale</strong>, <strong>senza conservanti, grassi animali né coloranti</strong>.";

$lang['portiamo-la-storia-title']       = "Portiamo con noi la Storia e la proiettiamo nel futuro";
$lang['portiamo-la-storia-content']     = "Da settant'anni <strong>la pizzetta di Trieste</strong> è un prodotto unico, punto di riferimento nella ristorazione veloce di qualità. È la pizzetta il prodotto del quale raccontiamo la storia attraverso le immagini antiche della famiglia Ciferni, dei produttori che da sempre ci accompagnano nella produzione e che all’estero sono icona di italianità, tradizione e artigianalità.
                                           <br />Nel format <strong>Tonda</strong> la storia è presente con i quadretti sui muri e con le cartoline che i clienti potranno portare con sé come ricordo di un’esperienza italiana.";

$lang['riccardo-caption']               = "<strong>Riccardo Ciferni</strong><br />Artigiano Pizzaiolo e Imprenditore";

$lang['riccardo-quote']                 = "<strong>Biologico</strong> non è solo una parola, ma la reale applicazione di una filosofia che trovi nei nostri prodotti.";
$lang['riccardo-signature']             = "Riccardo Ciferni";

$lang['gusto-locale-title']             = "Andiamo incontro al gusto locale.";
$lang['gusto-locale-content']           = "In ogni nazione andremo incontro al gusto a cui le persone sono più abituate, interpretando l’utilizzo di un ingrediente locale in una delle nostre pizzette. 
                                           Lo faremo con tutta la sapienza della cucina italiana, dando la possibilità di gustare qualcosa di unico.";


$lang['tradizione-nel-mondo-title']     = "Tradizione, Sapore, Ricerca e Rispetto. In tutto il mondo.";
$lang['tradizione-nel-mondo-content']   = "Riccardo, l’imprenditore che ha dato vita all’idea di replicare in tutto il mondo il gusto della pizzetta di suo padre Gabriele, diventa icona all’interno degli store <strong>Tonda</strong>, trasmettendo come la ricerca delle materie prime, l’artigianalità dei gesti e l’amore per il proprio lavoro siano ingredienti fondamentali per avere un prodotto eccezionale come la pizzetta.";

$lang['farina-caption']                 = "Farina italiana per <br /><strong>impasto certificato biologico</strong>";

$lang['da-produttori-locali']           = "da produttori locali";

$lang['packaging-title']                = "Il packaging dal calore italiano che tutti desiderano!";
$lang['packaging-content']              = "Il packaging che cotraddistingue Tonda porta con sé tutto il design italiano su prodotti che il pubblico di tutto il mondo è abituato ad usare e a vedere. Dalle shopping bag alle coffee cup, tutto sarà coordinato, trasmetterà l’idea di un brand solido, innovativo, fresco, internazionale.";
$lang['packaging-post-title']           = "Tonda. Not just an Italian sound";

$lang['freschi-e-italiani']             = "100% freschi e italiani";

$lang['finiture']                       = "materiali e finiture 100% naturali opaco";


$lang['prodotti-a-marchio-title']       = "Una linea di prodotti italiani a marchio <strong>Tonda</strong> da distribuire nei punti vendita.";
$lang['prodotti-a-marchio-content']     = "";

?>
