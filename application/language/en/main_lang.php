<?php

$lang['contenitore-di-idee']                = "A boxful of ideas";
$lang['subtitle-home']                      = "Intuizioni creative e capacità tecniche per creare prodotti che emozionano.";
$lang['guarda-il-video']                    = "LOOK THE VIDEO";
$lang['showreel']                           = "SHOWREEL";
$lang['chiedi-informazioni']                = "CHIEDI INFORMAZIONI";
$lang['i-nostri-eventi']                    = "OUR EVENTS";
$lang['vai-al-progetto']                    = "GO TO PROJECT";

$lang['progetto-per']                       = "Our project for";
$lang['cliente']                            = "Customer";
$lang['progetto']                           = "Project";
$lang['tutti-i-lavori']                     = "ALL PROJECTS";

$lang['foto-eventi-title']                  = "Some pics of our events";

$lang['grazie-per-aver-inviato']            = "Thanks for sending your request";

$lang['leggi-tutto']                        = "READ MORE";
$lang['scopri']                             = "SCOPRI";

$lang['nostro-stile']                       = "Do you like our style? Work with us!";
$lang['nome']                               = "FULL NAME*";
$lang['nazione']                            = "COUNTRY*";
$lang['company']                            = "COMPANY*";
$lang['ruolo']                              = "JOB TITLE*";
$lang['email']                              = "EMAIL*";
$lang['scrivi-richiesta']                   = "DETAILS OF THE PROJECT";
$lang['privacy']                            = "Ho letto e Accetto l'informativa sulla privacy";
$lang['invia']                              = "SEND MESSAGE ";

?>
