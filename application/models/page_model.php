<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $CI = & get_instance();
    }

    var $table = 'pages';  // modify here
    var $table_i18n = 'pages_i18n'; // modify here
    var $table_templates = 'pages_templates'; // modify here
    var $table_photogalleries = 'pages_photogalleries'; // modify here

    
    public function get_by_id($id) {
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row_array();
    }
    
    public function get_id_by_name($name) {
        $query = $this->db->get_where($this->table, array('name' => $name), 1, 0);
        return $query->row()->id;
    }
    
    public function get_name_by_id($id) {
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row()->name;
    }

    public function get_lang_by_id($id,$lang) {
        $query = $this->db->get_where($this->table_i18n, array('id' => $id,'lang' =>$lang), 1, 0);
        return $query->row_array();
    }
    
    public function get_page_full_content($name) {

        $this->db->join($this->table_templates, $this->table_templates.".id = ".$this->table.".template_id");
        $this->db->join($this->table_i18n, $this->table_i18n.".id = ".$this->table.".id");
        $this->db->where(array($this->table_i18n.'.lang' => $this->session->userdata('lang'),
                               $this->table.".published" => 1));
        $query = $this->db->get_where($this->table, array($this->table.".name" => $name));
        
        if($query->row() != null) {
            $result = $query->result_array();
            return $result[0];
        }
        else {
            return array();
        }
    }
    
    public function get_page_children_of($page_id) {
        $this->db->order_by('ord', 'ASC');
        $query = $this->db->get_where($this->table, array('parent_id' => $page_id, 'published' => true));
        return $query->result_array();
    }

    public function count() {
        $this->db->select('count(id) as record_count')->from($this->table);

        $record = $this->db->get();
        $row = $record->row();

        return $row->record_count;
    }

//    public function get($per_page, $offset, $sort_by) {
//
//        $query = $this->db->query("
//          SELECT ".$this->table.".id, ".$this->table.".date,".
//          $this->table.".city, ".$this->table.".who, ".$this->table.".visible 
//          FROM ".$this->table.
//          //" LEFT JOIN ".$this->table_i18n." ON ".$this->table_i18n.".id = ".$this->table.".id".
//          //" WHERE ".$this->table_i18n.".lang = '".$this->session->userdata['language']."'".
//          " ORDER BY ".$this->table.".".$sort_by." ASC".
//          " LIMIT ".$offset.", ".$per_page);
//        return $query->result_array();
//    }
//
//    public function get_page_content($name) {
//
//        $query = $this->db->query("
//          SELECT ".$this->table.".*, ".$this->table_i18n.".content
//          FROM ".$this->table." 
//          LEFT JOIN ".$this->table_i18n." ON ".$this->table_i18n.".id = ".$this->table.".id
//          WHERE ".$this->table.".name = '".$name."'
//          AND ".$this->table.".public = 1 
//          AND ".$this->table_i18n.".lang = '".$this->session->userdata('lang')."'"
//          );
//        
//        if($query->row() != null) {
//            $result = $query->result_array();
//            return $result[0]['content'];
//        }
//        else return "";
//    }
//    
//    public function get_page_title($name) {
//
//        $query = $this->db->query("
//          SELECT ".$this->table.".*, ".$this->table_i18n.".title
//          FROM ".$this->table." 
//          LEFT JOIN ".$this->table_i18n." ON ".$this->table_i18n.".id = ".$this->table.".id
//          WHERE ".$this->table.".name = '".$name."'
//          AND ".$this->table.".public = 1 
//          AND ".$this->table_i18n.".lang = '".$this->session->userdata('lang')."'"
//          );
//        
//        if($query->row() != null) {
//            $result = $query->result_array();
//            return $result[0]['title'];
//        }
//        else return "";
//    }
//    
//    

//    
//    public function get_page_siblings($page_id) {
//        $parent_id = $this->get_page_parent_id($page_id);
//        $query = $this->db->query("SELECT * FROM ".$this->table." WHERE parent_id = '".$parent_id."' ORDER BY ord ASC");
//        return $query->result_array();
//    }
//    
//    public function get_page_parent_id($page_id) {
//        $query = $this->db->query("SELECT parent_id FROM ".$this->table." WHERE id = ".$page_id);
//        $r = $query->row_array();
//        return $r['parent_id'];
//    }
//    
//    public function get_page_parent_name($page_id) {
//        $query = $this->db->query("SELECT parent_id FROM ".$this->table." WHERE id = ".$page_id);
//        $res = $query->row_array();
//        $query = $this->db->query("SELECT name FROM ".$this->table." WHERE id = ".$res['parent_id']);
//        $r = $query->row_array();
//        return $r['name'];
//    }

    
}

?>