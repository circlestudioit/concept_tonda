<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locations_model extends CI_Model {


    var $table                          = 'locations';  // Tabella madre
    var $table_i18n                     = 'locations_i18n';
    var $table_photogalleries           = 'photogalleries'; 
    var $table_countries                = 'countries'; // Tabella paesi
    var $table_countries_i18n           = 'countries_i18n'; // Tabella paesi i18n
    var $table_services                 = 'services';
    var $table_services_i18n            = 'services_i18n';
    var $table_locations_and_services   = 'locations_and_services';
    var $admins_table                   = 'admins'; // tabella degli admin
    
    
    public function get_location_full_content($name) {
       
        $this->db->join($this->table_i18n, $this->table_i18n.".id = ".$this->table.".location_id");
        $this->db->where(array($this->table_i18n.'.lang' => $this->session->userdata('lang')
                               /*,$this->table.".published" => 1*/));
        $query = $this->db->get_where($this->table, array($this->table.".loc_name" => $name));
        
        if($query->row() != null) {
            $result = $query->result_array();
            return $result[0];
        }
        else {
            return array();
        }
    }
    
    public function get_location_by_id($loc_id) {
       
        $this->db->join($this->table_i18n, $this->table_i18n.".id = ".$this->table.".location_id");
        $this->db->where(array($this->table_i18n.'.lang' => $this->session->userdata('lang')
                               /*,$this->table.".published" => 1*/));
        $query = $this->db->get_where($this->table, array($this->table.".location_id" => $loc_id));
        
        if($query->row() != null) {
            $result = $query->result_array();
            return $result[0];
        }
        else {
            return array();
        }
    }
    
    public function get_location_id_by_name($storename) {
        $this->db->select($this->table.'.location_id');
        $query = $this->db->get_where($this->table, array($this->table.'.loc_name' => $storename));
        $result = $query->result_array();
        return $result[0]['location_id'];
    }
    
    public function get_country_name_by_id($location_id) {
        $this->db->join($this->table_countries_i18n, $this->table_countries.'.id = '.$this->table_countries_i18n.'.id');
        $query = $this->db->get_where($this->table_countries, array($this->table_countries.'.id' => $location_id, $this->table_countries_i18n.'.lang' => $this->session->userdata('lang')));
        $result = $query->result_array();
        if(!empty($result))
            return $result[0]['country_name'];
        else
            return array();
    }
    
    public function get_services_for_location($storename) {
        
        $store_id = $this->get_location_id_by_name($storename);
        $this->db->select('service_id');
        $this->db->order_by('priority', 'ASC');
        $query1 = $this->db->get_where($this->table_locations_and_services, array($this->table_locations_and_services.'.location_id' => $store_id));
        $location_services = array();
        foreach ($query1->result() as $row) {
            array_push($location_services, $row->service_id);
        }
            
        if(!empty($location_services)) {
            $this->db->join($this->table_services_i18n, $this->table_services_i18n.".service_id = ".$this->table_services.".service_id");
            $this->db->where($this->table_services_i18n.'.lang', $this->session->userdata('lang'));
            $this->db->where_in($this->table_services.'.service_id', $location_services);
            $query = $this->db->get($this->table_services);

            $result = $query->result_array();
            if(!empty($result))
                return $result;
            else
                return array();
        }
        else return array();
    }

//    public function get_locations() {
//        // $this->db->group_by('country_name');
//        $this->db->order_by('country_name', 'ASC');
//        $this->db->join($this->table_countries, $this->table_countries.".id = ".$this->table.".country_id");
//        $query = $this->db->get($this->table);
//        return $query->result_array();
//    }
    
//    public function get_all_restaurants() {
//        // $this->db->group_by('country_name');
//        $this->db->order_by('country_name', 'ASC');
//        
//        $this->db->join($this->table_countries, $this->table_countries.".id = ".$this->table.".country_id");
//        $query = $this->db->get($this->table);
//        return $query->result_array();
//    }
    
    public function get_country_by_code($country_code) {
        $this->db->join($this->table_countries_i18n, $this->table_countries_i18n.'.id = '.$this->table_countries.'.id');
        $query = $this->db->get_where($this->table_countries, array($this->table_countries.'.country_code' => $country_code, 
                                                              $this->table_countries_i18n.'.lang' => $this->session->userdata('lang')));
        return $query->result_array();
    }
    
    public function get_restaurants_for_country($country_code) {
        
        $country = $this->get_country_by_code($country_code);
        $this->db->join($this->table_i18n, $this->table_i18n.".id = ".$this->table.".location_id");
        $this->db->order_by('ord', 'ASC');
        $this->db->select('store_short_name, loc_name, country_id, published, next_opening, is_active, city, store_name, lat, long, locations_i18n.address, description_2, thumb');
        $query = $this->db->get_where($this->table, array($this->table.'.country_id' => $country[0]['id'], 
                                                          $this->table.'.published' => 1,
                                                          //$this->table.'.is_active' => 1,
                                                          $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        return $query->result_array();
    }
    
    /* Gets all the countries */
    public function get_countries() {
        $this->db->select($this->table_countries_i18n.'.country_name, '.$this->table_countries_i18n.'.country_code, '.$this->table_countries.'.id');
        $this->db->distinct();
        $this->db->order_by($this->table_countries_i18n.'.country_name', 'ASC');
        $this->db->where($this->table_countries_i18n.'.lang', $this->session->userdata('lang'));
        //$this->db->join($this->table_countries, $this->table_countries.".id = ".$this->table.".country_id");
        $this->db->join($this->table_countries_i18n, $this->table_countries_i18n.".id = ".$this->table_countries.".id");
        $query = $this->db->get($this->table_countries);
        return $query->result_array();
    }
    
    /* Gets all the Active Countries - where at least one restaurant is open */
    public function get_all_countries() {
        $this->db->select($this->table_countries_i18n.'.country_name, '.$this->table_countries_i18n.'.country_code');
        $this->db->distinct();
        $this->db->order_by($this->table_countries_i18n.'.country_name', 'ASC');
        $this->db->where(array($this->table_countries_i18n.'.lang' => $this->session->userdata('lang'), $this->table_countries.'.active' => 1));
//        $this->db->join($this->table_countries, $this->table_countries.".id = ".$this->table.".country_id");
        $this->db->join($this->table_countries_i18n, $this->table_countries_i18n.".id = ".$this->table_countries.".id");
        $query = $this->db->get($this->table_countries);
        return $query->result_array();
    }
    
    
    public function geoget_restaurant($fLat, $fLon, $raggio) {
        $query = $this->db->query("
        SELECT
        ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( $fLat ) ) + COS( RADIANS( `lat` ) )
        * COS( RADIANS( $fLat )) * COS( RADIANS( `long` ) - RADIANS( $fLon )) ) * 6380 AS `distance`,
        location_id
        FROM ".$this->table." 
        WHERE
        ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( $fLat ) ) + COS( RADIANS( `lat` ) )
        * COS( RADIANS( $fLat )) * COS( RADIANS( `long` ) - RADIANS( $fLon )) ) * 6380 < ".$raggio." 
        ORDER BY `distance` LIMIT 1");
        
        return $query->result_array();
        
    }
}

?>