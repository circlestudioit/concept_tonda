<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Projects_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $CI = & get_instance();
    }

    var $table                          = 'projects';
    var $table_i18n                     = 'projects_i18n';
    var $table_photogalleries           = 'photogalleries';
    var $table_photogalleries_photos    = 'photos';
    var $table_photos_i18n              = 'photos_i18n';

    
    public function get_by_id($id) {
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row_array();
    }
    
    public function get_by_name($name) {
        $query = $this->db->get_where($this->table, array('name' => $name), 1, 0);
        return $query->row_array();
    }
    
    public function get_id_by_name($name) {
        $query = $this->db->get_where($this->table, array('name' => $name, 'published' => 1), 1, 0);
        if($query->row()!= null)
            return $query->row()->id;
        else return null;
    }
    
    public function get_name_by_id($id) {
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row()->name;
    }
    
    public function get_all() {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->order_by('date', 'DESC');
        $this->db->where(array('published' => 1, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        $this->db->join($this->table_i18n, $this->table.".id = ".$this->table_i18n.".id");
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_featured_projects() {
        $this->db->join($this->table_i18n, $this->table.".id = ".$this->table_i18n.".id");
        $this->db->where(array('published' => 1, 'hp' => 1, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        $this->db->order_by('date', 'DESC');
        $this->db->select('*');
        $this->db->from($this->table);
        
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_lang_by_id($id,$lang) {
        $query = $this->db->get_where($this->table_i18n, array('id' => $id,'lang' =>$lang), 1, 0);
        return $query->row_array();
    }

    public function get_full_content_by_id($id) {
        $this->db->select('*');
        //$this->db->from($this->table);
        $this->db->where(array($this->table_i18n.'.lang' => $this->session->userdata('lang')));
        $this->db->join($this->table_i18n, $this->table.".id = ".$this->table_i18n.".id");

        $query = $this->db->get_where($this->table, array($this->table.'.id' => $id), 1, 0);
//        $result = 
        return $query->row_array();
    }
    
    public function get_gallery_by_item_id($id) {
        $this->db->select('*');
        $this->db->order_by('ord', 'ASC');
        $this->db->join($this->table_photogalleries_photos, $this->table_photogalleries_photos.".photogallery_id = ".$this->table.".photogallery_id");

        $query = $this->db->get_where($this->table, array($this->table.'.id' => $id));
        return $query->result_array();
    }
    
    public function get_gallery_w_captions_by_item_id($id, $lang) {
        $this->db->select('*');
        $this->db->order_by('ord', 'ASC');
        $this->db->join($this->table_photogalleries_photos, $this->table_photogalleries_photos.".photogallery_id = ".$this->table.".photogallery_id");

        $query = $this->db->get_where($this->table, array($this->table.'.id' => $id));
        $gallery = $query->result_array();
        $complete_gallery = array();
        
        foreach($gallery as $pic) {
            $photo['photo'] = $pic;
            $photo['caption'] = $this->get_photo_caption($pic['photo_id'], $lang);
            array_push($complete_gallery, $photo);
        }
        
        return $complete_gallery;
    }
    
    public function get_gallery_by_id($gallery_id) {
        $this->db->select('photogallery_id');
        $this->db->from($this->table);
        $this->db->order_by('ord', 'ASC');
        $this->db->join($this->table_photogalleries_photos, $this->table.".photogallery_id = ".$this->table_photogalleries_photos.".photogallery_id");

        $query = $this->db->get_where($this->table, array('photogallery_id' => $gallery_id));
        return $query->result_array();
    }
    
    public function get_photo_caption($photo_id, $lang) {
//        $this->db->select('*');
//        $this->db->from($this->table_photos_i18n);
        
        $query = $this->db->get_where($this->table_photos_i18n, array('lang' => $lang, 'photo_id' => $photo_id), 1,0);
        return $query->row();
    }

    public function count() {
        $this->db->select('count(id) as record_count')->from($this->table);

        $record = $this->db->get();
        $row = $record->row();

        return $row->record_count;
    }
    
    
}

?>