<?php /* STORIA
<div class="zero">
    <div class="col-xs-12 coverimage" style="background-image: url(<?= base_url(IMAGES."bg.jpg"); ?>)">
        <h1 class="PF-Nexa grigio text-center story_title" ><?= $this->lang->line('story_title'); ?></h1>
        <div class="container-fluid text-left">
            <div class="hidden-xs col-sm-2">
                <img class="basilico" src="<?= base_url(IMAGES."basilico.png"); ?>" data-400-bottom="bottom: -200px" data--300-top="bottom: 0px;"  />
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="col-xs-6">
                   <img class="story_1" src="<?= base_url(IMAGES."story_1.png"); ?>" data-500-bottom="top: 14vw" data--200-top="top: 11vw" />
                   <h4 class="PF-Bariol grigio text-right story_caption" ><?= $this->lang->line('story_caption'); ?></h4>
                </div>
                <div class="col-xs-6">
                   <img class="story_2" src="<?= base_url(IMAGES."story_2.png"); ?>" width="70%" height="70%" data-400-bottom="top: 15vw" data--300-top="top: 10vw" />
                   <h4 class="PF-Bariol grigio text-right story_caption_right" ><?= $this->lang->line('story_caption_right'); ?></h4>
                </div>
            </div>
            <div class="hidden-xs col-sm-2">
                <img class="margherita hidden-xs" src="<?= base_url(IMAGES."margherita.png"); ?>" data-400-bottom="margin-top:-100px" data--1000-top="margin-top: 0px;" />
            </div>
        </div>
    </div>
</div>
*/ ?>
<div class="zero">
    <div class="col-xs-12 coverimage" style="background-image: url(<?= base_url(IMAGES."header.jpg"); ?>)">
        <div class="container-fluid text-left">
            <div class="content-vcenter">
                <h1 class="PF-BigCaslon white bigger text-left bottomTopOpenDelay" data-0="margin-top: 0px" data-500="margin-top: 200px;">
                    <?= $this->lang->line('header_title'); ?>
                </h1>
            </div>
        </div>
    </div>
</div>
<div class="zero">
    <div class="col-xs-12" style="background-color: #1f1f1f;">
        <div class="container-fluid content-text bottomtop">
            <div class=" col-xs-12 col-sm-6 col-sm-offset-3 full-width-text">
                <h1 class="PF-Bariol white title text-center ">
                    <?= $this->lang->line('obiettivo_title'); ?>
                </h1>
                <p class="PF-Bariol white text-center text-full challenge ">
                    <?= $this->lang->line('obiettivo_content'); ?>
                </p>
            </div>
        </div>
    </div>
</div>
 <div class="zero">
    <div class="col-xs-12">
        <div class="container-fluid content-text">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 full-width-text">
                <img class="img-responsive center-block" src="<?= base_url(IMAGES."tonda_logo_concept.png"); ?>" style="margin: 3em auto" />
            </div>
        </div>
    </div>
</div> 

<div class="zero">
    <div class="col-xs-12" style="background-color: #e53946;">
        <div class="container-fluid content-text ">
            <div class="hidden-xs col-sm-2">
                <img class="basilico" src="<?= base_url(IMAGES."basilico.png"); ?>" data-400-bottom="bottom: -200px" data--300-top="bottom: 0px;"  />
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-1 full-width-text bottomtop">
                <h1 class="PF-Bariol white title text-center ">
                    <?= $this->lang->line('soluzione_title'); ?>
                </h1>
                <p class="PF-Bariol white text-justify text-full challenge ">
                    <?= $this->lang->line('soluzione_content'); ?>
                    <?= $this->lang->line('format_content'); ?>
                </p>
                <h3 class="PF-Bariol white text-center">
                    <?= $this->lang->line('authenticorganic'); ?>
                </h3>
            </div>
            <div class="hidden-xs col-sm-2">
                <img class="pomodorini" src="<?= base_url(IMAGES."pomodorini.png"); ?>" data-400-bottom="bottom: -400px" data--300-top="bottom: -200px;" />
            </div>
        </div>
    </div>
</div>


<!-- <div class="zero">
    <div class="col-xs-12" style="background-image: url(<?= base_url(IMAGES."bg_2.png"); ?>); background-size: contain; background-repeat: repeat-y;">
        <div class="container-fluid content-text">
            <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4 full-width-text">
                <h2 class="PF-Bariol white text-center text-full ">
                    <?= $this->lang->line('format_content'); ?>
                </h2>
            </div>
        </div>
    </div>
</div> -->

<div class="zero">
    <div class="col-xs-12">
        <!--<img src="<?= base_url(IMAGES."tonda-restaurant.jpg"); ?>" class="img-responsive" />-->
        <img src="<?= base_url(IMAGES."render-1.jpg"); ?>" class="img-responsive" />
    </div>
</div>

<div class="zero">
    <div class="col-xs-12">
        <div class="container-fluid content-text">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3 full-width-text">
                <h2 class="PF-Bariol dark text-center text-full ">
                    <?= $this->lang->line('offerta-chiara-title'); ?>
                </h2>
                <p class="PF-Bariol dark text-justify text-full ">
                    <?= $this->lang->line('offerta-chiara-content'); ?>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="zero">
    <div class="col-xs-12">
        <img src="<?= base_url(IMAGES."render-2.jpg"); ?>" class="img-responsive" />
    </div>
</div>




<div class="zero">
    <div class="col-xs-12">
        <img src="<?= base_url(IMAGES."vetrofania-cibi.jpg"); ?>" class="img-responsive" data-500-top="margin-left: -100px" data-200-top="margin-left: 0" />
    </div>
</div>


<div class="zero">
    <div class="container-fluid col-xs-12">
        <div class="col-xs-12 col-sm-8" id="i_am_tonda">
            <img src="<?= base_url(IMAGES."render-3.jpg"); ?>" class="img-responsive" />
        </div>

        <div class="col-xs-12 col-sm-4 tondared">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2" id="accoglienza">
                <h2 class="PF-Bariol white text-center text-full ">
                    <?= $this->lang->line('accoglienza-title'); ?>
                </h2>
                <p class="PF-Bariol white text-justify text-full ">
                    <?= $this->lang->line('accoglienza-content'); ?>
                </p>
                <br /><br />
                <h2 class="PF-Bariol white text-center text-full ">
                    <?= $this->lang->line('accoglienza-post-title'); ?>
                </h2>
            </div>
        </div> 
    </div>
</div>

<div class="zero">
    <div class="col-xs-12">
        <div class="col-xs-12 col-sm-6">
            <br /><br />
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 sfl" style="padding-left: 15px !important; padding-right: 15px !important">
                <h1 class="PF-Nexa big red text-left">
                    <?=$this->lang->line('benvenuto-in-italia')?>
                </h1>
                <p class="PF-Bariol">
                    <?=$this->lang->line('benvenuto-in-italia-content')?>
                </p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <img src="<?=base_url(IMAGES."apetta.png")?>" class="apetta pull-right img-responsive" data-200-top="right: -10%" data-600-top="right: -15%" />
        </div>
    </div>
</div>

<div class="zero" style="margin-top: 5em;">
    <div class="col-xs-12" style="background:#000; padding-top: 2em !important; padding-bottom: 5em !important;">
        <div class="col-xs-12 col-sm-6">
            <img src="<?=base_url(IMAGES."render-4.jpg")?>" class="center-block img-responsive" />
        </div>
        <div class="col-xs-12 col-xs-6">
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2" style="padding-left: 15px !important; padding-right: 15px !important">
                <br /><br />
                <h1 class="white PF-Bariol">
                    <?=$this->lang->line('the-dubai-store-title')?>
                </h1>
                <hr /><br /><br />
                <p class="white" style="line-height: 2em;">
                    <?=$this->lang->line('the-dubai-store-content')?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="zero bg-pattern" style="position: relative; z-index: 10;">
    <div class="container-fluid">
        <div class="col-xs-12 col-sm-6">
            <img src="<?=base_url(IMAGES."quadretti.jpg")?>" class="img-responsive sfl" />
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <img src="<?=base_url(IMAGES."trieste-mare.jpg")?>" class="center-block trieste-mare fadeIn" />
            </div>
            <div class="clear"></div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                <div class="col-xs-12 col-sm-12 padding-top-bottom">
                    <h2 class="PF-Bariol dark text-left text-full ">
                        <?= $this->lang->line('portiamo-la-storia-title'); ?>
                    </h2>
                    <p class="PF-Bariol dark text-justify text-full ">
                        <?= $this->lang->line('portiamo-la-storia-content'); ?>
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <img src="<?=base_url(IMAGES."freccia-tonda.png")?>" class="center-block freccia-tonda" />
            </div>
        </div>
    </div>
</div>

<div class="zero">
    <div class="col-xs-12 col-sm-12" style="height: 20em">
        <div class="col-xs-12 col-sm-6">
            <p class="col-xs-8 col-sm-12 text-right farina-caption PF-Bariol">
                <?=$this->lang->line('farina-caption')?>
            </p>
        </div>
        <div class="col-xs-12 col-sm-6" style="height: 20em">
            <img src="<?=base_url(IMAGES."farina.jpg")?>" class="farina" />    
        </div>
        <!--<img src="<?=base_url(IMAGES."pomodoro.png")?>" class="pomodoro" />-->
    </div>
</div>

<div class="zero">
    <div class="container-fluid col-xs-12">
        <div class="col-xs-12 col-sm-6 col-md-8">
            <img src="<?=base_url(IMAGES."riccardo.jpg")?>" class="img-responsive riccardo-foto" />
            <p class="riccardo-caption PF-Bariol white sfl">
                <?=$this->lang->line('riccardo-caption')?>
            </p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4" style="padding-left: 15px !important; padding-right: 15px !important;">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                <h2 class="PF-Bariol dark text-left text-full ">
                    <?= $this->lang->line('tradizione-nel-mondo-title'); ?>
                </h2>
                <p class="PF-Bariol dark text-justify text-full ">
                    <?= $this->lang->line('tradizione-nel-mondo-content'); ?>
                </p>
                <p class="PF-BigCaslon green text-center text-full " style="padding-top: 2em !important">
                    <span class="big PF-BigCaslon openquotes">,,</span>
                    <span class="riccardo-quote">
                        <?= $this->lang->line('riccardo-quote'); ?>
                    </span>
                    <span class="big PF-BigCaslon closedquotes">,,</span>
                    <br /><br />
                    <span class="text-center PF-Nexa" style="font-size: 1.7em; position: relative; z-index: 9;">Riccardo Ciferni</span>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="zero tondared" style="height: 20em">
    <div class="container-fluid col-xs-12" style="margin: 3em 0">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-lg-6 col-lg-offset-3">
            <h1 class="PF-Bariol white text-center big ">
                <?=strtoupper($this->lang->line('offerta-title'));?>
            </h1>
            <p class="PF-Bariol white text-center text-full ">
                <?=$this->lang->line('offerta-concept');?>
            </p>
        </div>
    </div>
</div>

<div class="zero">
    <div class="col-xs-12">
        <div class="col-xs-12 col-sm-5 artigiani-posters" id="artigiani-poster" style="background-image: url(<?=base_url(IMAGES."artigiani-pizzaioli.jpg")?>)">
        </div>
        <div class="col-xs-12 col-sm-7 flexslider" id="slider-pizzerie">
            <ul class="slides">
                <li class="slide">
                    <img src="<?=base_url(IMAGES."slider-image-1.jpg")?>" class="img-responsive" />
                </li>
                <li class="slide">
                    <img src="<?=base_url(IMAGES."slider-image-2.jpg")?>" class="img-responsive" />
                </li>
<!--                <li class="slide">
                    <img src="<?=base_url(IMAGES."slider-image-3.jpg")?>" class="img-responsive" />
                </li>-->
            </ul>
        </div>
    </div>
</div>

<div class="zero">
    <div class="col-xs-12">
        <div class="col-xs-12 col-sm-5">
            <img src="<?=base_url(IMAGES."lasagna.jpg")?>" class="img-responsive sfl" />
        </div>
        <div class="col-xs-12 col-sm-7">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <br /><br />
                <h2 class="PF-Bariol dark text-left text-full ">
                    <?= $this->lang->line('offerta-varia-title'); ?>
                </h2>
                <p class="PF-Bariol dark text-justify text-full ">
                    <?= $this->lang->line('offerta-varia-content'); ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="zero">
    <div class="col-xs-12">
        <div class="col-xs-12 col-sm-5">
            <img src="<?=base_url(IMAGES."panini.jpg")?>" class="img-responsive" />
        </div>
        <div class="col-xs-12 col-sm-7">
            <img src="<?=base_url(IMAGES."insalata.jpg")?>" class="img-responsive" />
        </div>
    </div>
</div>

    
<div class="zero">
    <div class="container-fluid col-xs-12">
        <div class="col-xs-5">
            <img src="<?=base_url(IMAGES."fastmenu.jpg")?>" class="center-block col-xs-12" />
        </div>
        <div class="col-xs-7">
            <!--<img src="<?=base_url(IMAGES."organic.png")?>" class="center-block col-xs-12 col-sm-4 col-sm-offset-4 organic-wall" />-->
            <img src="<?=base_url(IMAGES."pizzette-wall.jpg")?>" class="img-responsive pizzette-wall" />
            <p class="col-xs-12 PF-BigCaslon green la-nostra-pizzetta sfl"><?=$this->lang->line('la-nostra-pizzetta')?></p>
        </div>
    </div>
</div>
    
<div class="zero">
    <div class="container-fluid col-xs-12">        
        <div class="col-xs-12 col-sm-5" id="gusto-locale-container">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2" style="z-index: 0; padding-left: 15px !important; padding-right: 15px !important;">
                <br />
                <h2 class="PF-Bariol dark text-left text-full ">
                    <?= $this->lang->line('gusto-locale-title'); ?>
                </h2>
                <p class="PF-Bariol dark text-justify text-full ">
                    <?= $this->lang->line('gusto-locale-content'); ?>
                </p>
                
            </div>
            <div class="hidden-xs col-sm-6 pull-left bottomabs">
                <img src="<?=base_url(IMAGES."pizzetta-blue-stilton.png")?>" class="pizzetta-stilton" />
            </div>
            <div class="hidden-xs col-sm-6 pull-right bottomabs" style="right: 0; margin-bottom: -20%">
                <p class="PF-Nexa red text-right stilton-caption">
                    English<br />Blue Stilton<br />
                    <span class="PF-Bariol dark produttori-locali">
                        <?=$this->lang->line('da-produttori-locali')?>
                    </span>
                </p>
                <img src="<?=base_url(IMAGES."blue-stilton.png")?>" class="stilton img-responsive" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-7" id="londra">
            <img src="<?=base_url(IMAGES."londra.jpg")?>" class="img-responsive" />
        </div>
    </div>
</div>
    
<div class="zero hidden-xs">
    <div class="col-xs-12 container-fluid friarielli-container">
        <div class="col-xs-12 col-sm-6" id="friarielli_caption">
            <p class="PF-Nexa green friarielli-caption">
                Friarielli<br />
                <span class="PF-Bariol dark produttori-locali">
                    <?=$this->lang->line('freschi-e-italiani')?>
                </span>
            </p>
        </div>
        <div class="col-xs-12 col-sm-6" id="friarielli">
            <img src="<?=base_url(IMAGES."friarielli.png")?>" class="friarielli img-responsive" />
        </div>
        
    </div>
</div>

<div class="zero">
    <div class="col-xs-12 container-fluid packaging-back" style="padding-top: 3em !important; padding-bottom: 3em !important">
        <div class="col-xs-12 col-sm-3">
            <!--<img src="<?=base_url(IMAGES."tonda-bianco.png")?>" class="center-block tonda-bianco sfl" />-->
        </div>
        <div class="col-xs-12 col-sm-6 full-width-text" style="padding-left: 15px !important; padding-right: 15px !important">
            <h2 class="PF-Bariol white text-left text-full ">
                <?= $this->lang->line('packaging-title'); ?>
            </h2>
            <p class="PF-Bariol white text-justify text-full ">
                <?= $this->lang->line('packaging-content'); ?>
            </p>
            <br />
            <h3 class="PF-Bariol white text-center text-full ">
                <?= $this->lang->line('packaging-post-title'); ?>
            </h3>
        </div>
    </div>
</div>

<div class="zero">
    <div class="container-fluid col-xs-12">
        <div class="col-xs-12">
            <img src="<?=base_url(IMAGES."paper_bag.jpg")?>" class="img-responsive" />
        </div>
        <div class="col-xs-12">
            <img src="<?=base_url(IMAGES."bicchieri.jpg")?>" class="img-responsive" />
        </div>
    </div>
</div>


<div class="zero">
    <div class="container-fluid col-xs-12">
        <div class="col-xs-12 col-sm-4" id="salad" style="background-image: url(<?=base_url(IMAGES."salad.jpg")?>); background-size: cover; background-repeat: no-repeat; background-position: center">
            
        </div>
        <div class="col-xs-12 col-sm-8 " id="salad_right">
            <img src="<?=base_url(IMAGES."salad_parmigiana_zuppa.jpg")?>" class="img-responsive" />
            
<!--            <h1 class="PF-Bariol white text-center text-full">
                Buon appetito!
            </h1>
            <img src="<?=base_url(IMAGES."posate.jpg")?>" class="img-responsive center-block" />
            <h3 class="PF-Bariol white text-center text-full">
                <?=$this->lang->line('finiture');?>
            </h3>-->
        </div>
    </div>
</div>
    
<div class="zero bg-pattern" id="coffee-anchor">
    <div class="col-xs-12 col-sm-12">
        <h1 class="PF-Bariol green text-center">
            ORGANIC<br />coffee
        </h1>
        <div class="col-xs-10 col-xs-offset-2 col-sm-5 col-sm-offset-0">
            <div class="text-left col-xs-12 col-sm-6 col-sm-offset-3">
                <h1 class="PF-Nexa red big">
                    International
                </h1>
                <h1 class="PF-Bariol red big" style="margin-top: -.3em">
                    coffee
                </h1>
            </div>
        </div>
        <div class="col-xs-6 col-xs-offset-3 col-sm-2 col-sm-offset-0">
            <img src="<?=base_url(IMAGES."coffee-icon.png")?>" class="center-block img-responsive" />
        </div>
        <div class="col-xs-10 col-xs-offset-2 col-sm-5 col-sm-offset-0">
            <div class="text-left col-xs-12 col-sm-6 col-sm-offset-3">
                <h1 class="PF-Nexa red big">
                    Italian
                </h1>
                <h1 class="PF-Bariol red big" style="margin-top: -.3em">
                    espresso
                </h1>
            </div>
            <div class="hidden-xs col-sm-3">
                <img src="<?=base_url(IMAGES."freccia-sotto.png")?>" class="pull-right freccia-caffe" />
            </div>
        </div>
    </div>
</div>
<div class="zero">
    <div class="col-xs-12 container-fluid">
        <div class="col-xs-12 col-sm-6">
            <img src="<?=base_url(IMAGES."mug.png")?>" class="img-responsive center-block" />
        </div>
        <div class="col-xs-12 col-sm-6">
            <img src="<?=base_url(IMAGES."espresso.jpg")?>" class="img-responsive" />
        </div>
    </div>
</div>

<div class="zero">
    <div class="container-fluid col-xs-12">
        <div class="col-xs-12 col-sm-12">
            <img src="<?=base_url(IMAGES."divise.jpg")?>" class="img-responsive" />
        </div>
    </div>
</div>
    
<div class="zero">
    <div class="container-fluid col-xs-12">
        <div class="col-xs-12 col-sm-6">
            <img src="<?=base_url(IMAGES."bags-left.jpg")?>" class="img-responsive" />
        </div>
        <div class="col-xs-12 col-sm-6">
            <img src="<?=base_url(IMAGES."bags-right.jpg")?>" class="img-responsive" />
        </div>
    </div>
</div>
    

<div class="zero">
    <div class="container-fluid col-xs-12">
        <div class="col-xs-12 col-sm-12 tondared" style="padding-top: 3em !important; padding-bottom: 3em !important" id="prodottiamarchio">
            <div class="col-xs-12 col-sm-12">
                <img src="<?=base_url(IMAGES."tonda-bianco.png")?>" class="center-block tonda-bianco" />
            </div>
            <div class="col-xs-12 col-sm-12 full-width-text" style="padding-left: 15px !important; padding-right: 15px !important">
                <h2 class="PF-Bariol white text-center text-full">
                    <?= $this->lang->line('prodotti-a-marchio-title'); ?>
                </h2>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12" id="prodotti_a_marchio_container">
            <img src="<?=base_url(IMAGES."prodotti-a-marchio.jpg")?>" class="img-responsive" />
        </div>
    </div>
</div>

    
    
<div class="zero">
    <div class="col-xs-12" style="margin: 5em 0 2em 0">
        <div id="video">
            <?php if($this->session->userdata('lang') == 'it'): ?>
                <iframe src="https://player.vimeo.com/video/240354211" width="" height="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <?php else: ?>
                <iframe src="https://player.vimeo.com/video/240354211" width="" height="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <?php endif; ?>
        </div>
    </div>
</div>