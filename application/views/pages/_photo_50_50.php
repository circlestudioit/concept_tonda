<div class="zero">
    <div class="col-xs-12">
        <div class="masonry">
            <?php foreach ($gallery as $photo): ?>
                   <div class="item coverimage col-xs-6" style=" background-image: url(<?= base_url($this->config->item('photo_image').$photo['image']) ?>);"></div>     
            <?php endforeach; ?>
        </div> 
    </div>
</div>
