<div class="zero">
    <div class="col-xs-12">
        <div id="video">
            <?php if($this->session->userdata('lang') == 'it'): ?>
                <iframe src="https://player.vimeo.com/video/201509983" width="" height="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <?php else: ?>
                <iframe src="https://player.vimeo.com/video/206393763" width="" height="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <?php endif; ?>
        </div>
    </div>
</div>