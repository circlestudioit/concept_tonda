<div class="zero">
    <div class="col-xs-12">
        <div class="text-center">

            <div class="col-xs-6 content-vcenter">
                <div class="col-xs-6 col-xs-offset-3">
                    <h1 class="PF-BlackItalic dark bigger text-center">
                        <?= $title ?>
                    </h1>
                    <h3 class="PF-Regular dark text-center">
                        <?= $content ?>
                    </h3>
                </div>
            </div>
            <div class="col-xs-6 coverimage" style="background-image: url(<?= base_url($this->config->item('pages_image').$image) ?>)">
            </div>

        </div>
    </div>
</div>