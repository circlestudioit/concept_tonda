<div class="zero">
    <div class="col-xs-12" style="background-color: #<?= $meta_keywords; ?>">
        <div class="container-fluid content-text">
            <div class="col-xs-6 col-xs-offset-3 full-width-text">
                <h1 class="PF-Bariol white title text-center">
                    <?= $title ?>
                </h1>
                <p class="PF-Bariol white text-left text-full">
                    <?= $content ?>
                </p>
            </div>
        </div>
    </div>
</div>
