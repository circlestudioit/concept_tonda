<div class="zero">
    <div class="col-xs-12 coverimage" style="background-image: url(<?= base_url($this->config->item('pages_image').$image) ?>)">
        <div class="container-fluid text-left">
            <div class="content-vcenter" style="padding: 60px !important">
                <h1 class="PF-BigCaslon white bigger text-left">
                    <?= $title ?>
                </h1>
                <h3 class="PF-Bariol white text-left">
                    <?= $headline ?>
                </h3>
            </div>
        </div>
    </div>
</div>