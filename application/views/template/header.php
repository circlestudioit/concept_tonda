<header class="header-wrapper">
    <div class="col-xs-12 col-sm-2">
        <a href="<?=base_url()?>">
        <img src="<?=base_url(IMAGES."circle-studio-logo.png")?>" class="logo-home" /></a>
    </div>
    <div class="col-xs-12 col-sm-8">
        <?php echo $nav; ?>
    </div>
    <div class="hidden-xs col-sm-2 pull-right languagemenu">
        <a href="<?=base_url("lang/it")?>" class="neutral">IT</a> | <a href="<?=base_url("lang/en")?>" class="neutral">EN</a>
    </div>
    <div class="hidden-xs col-sm-1"></div>
</header>
