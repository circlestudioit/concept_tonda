<nav id="nav-home" class="navbar">
    <ul class="menu-items col-xs-12 col-sm-12 text-center">
<!--            <li>
                <a class="neutral linkmainnav link--kukurimainnav" data-letters="<?=$this->lang->line('home')?>" href="<?=base_url()?>">
                <?=$this->lang->line('home')?></a>
            </li>-->
            <li>
                <a class="neutral linkmainnav link--kukurimainnav" data-letters="<?=$this->lang->line('cosa-facciamo')?>" href="<?=base_url("what-we-do")?>">
                <?=$this->lang->line('cosa-facciamo')?></a>
            </li>
            <li>
                <a class="neutral linkmainnav link--kukurimainnav" data-letters="<?=$this->lang->line('lavori')?>" href="<?=base_url("works")?>">
                <?=$this->lang->line('lavori')?></a>
            </li>
            <li>
                <a class="neutral linkmainnav link--kukurimainnav" data-letters="<?=$this->lang->line('eventi')?>" href="<?=base_url("events")?>">
                <?=$this->lang->line('eventi')?></a>
            </li>
            <li>
                <a class="neutral linkmainnav link--kukurimainnav" data-letters="<?=$this->lang->line('riconoscimenti')?>" href="<?=base_url("awards")?>">
                <?=$this->lang->line('riconoscimenti')?></a>
            </li>
            <li>
                <a class="neutral linkmainnav link--kukurimainnav" data-letters="<?=$this->lang->line('contatti')?>" href="<?=base_url("contact")?>">
                <?=$this->lang->line('contatti')?></a>
            </li> 
        </ul>    
</nav>