<footer id="footer" class="footer-interne text-center col-xs-12 col-sm-12">
    <div class="hidden-xs col-sm-1"></div>
    <div class="col-xs-12 col-sm-3 text-left">
        <h3 class="lato green">
            MENU
        </h3>
        <a href="<?=base_url()?>">HOME</a><br />
        <a href="<?=base_url('what-we-do')?>"><?=$this->lang->line('cosa-facciamo')?></a><br />
        <a href="<?=base_url('works')?>"><?=$this->lang->line('lavori')?></a><br />
        <a href="<?=base_url('events')?>"><?=$this->lang->line('eventi')?></a><br />
        <a href="<?=base_url('awards')?>"><?=$this->lang->line('riconoscimenti')?></a><br />
        <a href="<?=base_url('contact')?>"><?=$this->lang->line('contatti')?></a><br />
        <h3 class="lato green" style="margin-top: 2em">
            COMPANY
        </h3>
        Circle Studio srl<br />
        <i>Main Office</i><br />
        Via Tintoretto, 11<br />
        65123 PESCARA (PE) ITALY<br />
        <a href="tel:003908572590" class="neutral">+3908572590</a> \ <a href="mailto:hello@circlestudio.it" class="neutral">hello@circlestudio.it</a><br />
        P.IVA / VAT n. 02114060680
        
    </div>
    
    <div class="col-xs-12 col-sm-4">
        <img src="<?=base_url(IMAGES."circle-studio-logo-footer.png")?>" class="logo-footer" />
    </div>
    <div class="col-xs-12 col-sm-3 text-right">
        <h3 class="lato green text-right">
            WHAT'S UP
        </h3>
        <?php foreach($posts as $post): ?>
        <a href="<?=base_url('news/'.$post['url'])?>">
            <?=$post['title'] ?>
        </a>
        <br />
        <?php endforeach; ?>
        <h3 class="lato green text-right" style="margin-top: 2em">
            SOCIAL
        </h3>
        <a href="http://www.facebook.com/circlestudio.it" target="_blank">Facebook</a><br />
        <a href="http://www.twitter.com/circlestudi0" target="_blank">Twitter</a><br />
        <a href="http://www.instagram.com/circle_studio" target="_blank">Instagram</a><br />
        <a href="http://www.linkedin.com/company/2909705" target="_blank">LinkedIn</a><br />
        <a href="https://www.youtube.com/channel/UCy8mOYhR7OOW-N1kM7VwRYQ" target="_blank">YouTube</a><br />
        <a href="https://vimeo.com/circlestudi0" target="_blank">Vimeo</a>
    </div>
    <div class="hidden-xs col-sm-1"></div>
    <div class="clear col-xs-12 col-sm-12">
        &COPY; 2017 
        <a class="neutral" href="http://www.circlestudio.it" target="_blank"><strong>CIRCLE</strong>STUDIO</a>
    </div>
    
</footer>