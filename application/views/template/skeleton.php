<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php echo $title ?></title>

<meta name="viewport" content="width=device-width">
<meta name="keywords" content="<?php echo isset($keywords) ? $keywords : ''; ?>" />
<meta name="author" content="<?php echo isset($author) ? $author : ''; ?>" />
<meta name="description" content="<?php echo isset($description) ? $description : ''; ?>" />

<meta property="og:title" content="<?php echo $title ?>" />
<meta property="og:site_name" content=""/>
<meta property="og:url" content="<?=current_url()?>" />
<meta property="og:description" content="<?php echo isset($description) ? $description : ''; ?>" />
<meta property="og:type" content="article" />
<meta property="article:author" content="" />
<meta property="article:publisher" content="" />
<?php 
    if(isset($pagecontent['cover']) && $pagecontent['cover']) {
        $image = $pagecontent['cover'];
        $folder = $this->config->item('pages_cover');
    }
    elseif (isset($pagecontent['thumb']) && $pagecontent['thumb']) {
        $image = $pagecontent['thumb'];
        $folder = $this->config->item('pages_thumb');
    }
    elseif (isset($pagecontent['image']) && $pagecontent['image']) {
        $image = $pagecontent['image']; 
        $folder = $this->config->item('pages_image');
    }
?>
<meta property="og:image" content="<?php if(isset($image) && $image != '') echo base_url($folder.$image); else echo base_url(IMAGES."1200x630-fb.jpg"); ?>" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="630" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="<?php echo base_url(JS."libs/html5shiv.js");?>"></script>
<![endif]-->

<style>
    /* Preloader */

    #preloader {
        position: fixed;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-color:#FFF; /* change if the mask should have another color then white */
        z-index:9999999; /* makes sure it stays on top */
    }

    #status {

    }
</style>

<!-- Le fav and touch icons -->
<!--<link rel="shortcut icon" href="<?php echo base_url(IMAGES.'favicon.ico');?>">-->
<link rel="icon" type="image/png" href="<?php echo base_url(IMAGES.'favicon.png');?>" />

</head>
<body>   
<!--Preloader--> 
<div id="preloader">
    <div id="status">
        
    </div>
</div>

<!--<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&appId=284835101696170&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>-->


	<?php echo $body ?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<!--        <script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/jquery.effects.core.js"></script>
        <script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/jquery.effects.slide.js"></script>-->
        <!-- Latest compiled and minified Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>  
        
        <?php
//        if (!$detect->isMobile()):
//        ?>
        <script src="<?php echo base_url(JS."libs/velocity.min.js")?>"></script>
        <script src="<?php echo base_url(JS."libs/velocity.ui.min.js")?>"></script>
        <script src="<?php echo base_url(JS."config-velocity.min.js")?>"></script>
        <script src="<?php echo base_url(JS."animations-velocity.js")?>"></script>
        <script src="https://player.vimeo.com/api/player.js"></script>
        <?php //endif; ?>
        
        <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>-->
        
	<script>window.jQuery || document.write('<script src="<?php echo base_url(JS."libs/jquery-1.10.1.min.js");?>"><\/script>')</script>
        <script src="<?php echo base_url(JS."plugins.js");?>"></script>
<!--        <script src="//cdn.jsdelivr.net/jquery.scrollto/2.1.0/jquery.scrollTo.min.js"></script>-->
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=base_url().CSS."bootstrap-xxl.css"?>">

        <!-- extra CSS-->
        <?php foreach($css as $c):?>
        <link rel="stylesheet" href="<?php echo base_url().CSS.$c?>">
        <?php endforeach;?>

        <!-- extra fonts-->
        <?php foreach($fonts as $f):?>
        <link href="http://fonts.googleapis.com/css?family=<?php echo $f?>"
                rel="stylesheet" type="text/css">
        <?php endforeach;?>
        
	<!-- extra js-->
	<?php foreach($javascript as $js):?>
	<script defer src="<?php echo base_url().JS.$js?>"></script>
	<?php endforeach;?>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-47701604-3', 'auto');
      ga('send', 'pageview');

    </script>


</body>
</html>