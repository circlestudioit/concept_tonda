<header class="header-wrapper-interne">
    <div class="col-xs-4 col-sm-2">
        <a href="<?=base_url()?>">
        <img src="<?=base_url(IMAGES."circle-studio-logo.png")?>" class="logo-home" /></a>
    </div>
    
    <div class="col-xs-6 col-sm-2 pull-right" style="padding: 0">
        <div id="menu-icon" class="menuscritto openmenu lato expanded bold dark" style="padding-left: 15px">
            <div class="base <?php if(isset($menucolor)) echo $menucolor ?>">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <div class="hidden-xs col-sm-1"></div>
    
    <?php echo $nav ?>
</header>
