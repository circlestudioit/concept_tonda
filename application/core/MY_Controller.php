<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
    //Page info
    protected $data = Array();
    protected $pageName = FALSE;
    protected $template = "main";
    protected $hasNav = TRUE;
    //Page contents
    protected $javascript = array();
    protected $css = array();
    protected $fonts = array();
    //Page Meta
    protected $title = FALSE;
    protected $description = FALSE;
    protected $keywords = FALSE;
    protected $author = FALSE;
    protected $detect = '';
	
    function __construct()
    {

            parent::__construct();
//            $this->data["uri_segment_1"] = $this->uri->segment(1);
//            $this->data["uri_segment_2"] = $this->uri->segment(2);
            $this->title = $this->config->item('site_title');
            $this->description = $this->config->item('site_description');
            $this->keywords = $this->config->item('site_keywords');
            $this->author = $this->config->item('site_author');
            
            $this->session->set_userdata('currentPage', current_url());

            $supportedLangs = $this->config->item('active_langs');
            $check_language = (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : $this->config->item('l');  
            $languages = explode(',',substr($check_language, 0, 2));
                
            if(!$this->session->userdata('userlang')) {
                foreach($languages as $lang)
                {
                    if(in_array($lang, $supportedLangs))
                    {
                    // Set the page locale to the first supported language found
                    $this->session->set_userdata('lang', $lang);
                    setlocale(LC_ALL, $lang);
                    break;

                    }
                    else {
                        //Imposto la lingua di default
                        $this->session->set_userdata('lang', $this->config->item('language'));
                        setlocale(LC_ALL, $this->config->item('language'));
                    }
                }
            }

            $this->pageName = strToLower(get_class($this));

            $this->load->library('form_validation');
            $this->load->library('Mobile_Detect');
            
            $this->lang->load('main', $this->session->userdata('lang'));
            $this->lang->load('menu', $this->session->userdata('lang'));
            $this->lang->load('error', $this->session->userdata('lang'));

            $this->load->model('page_model');
            $this->load->model('gallery_model');
            $this->load->model('post_model');
            $this->load->model('projects_model');
            //$this->load->model('locations_model');
            $this->load->model('menu_model');
            //$this->load->model('province_model');
            $this->load->helper('text');
            
            $this->detect = new Mobile_Detect();
            
    }
        
        
    /****************************
     * 
     *           HOME
     * 
     ***************************/
        
        /* render pagina principale  */
    protected function _render($view) {
        
        $this->session->set_userdata('currentPage', current_url());
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        //meta
        
        $toTpl["author"] = $this->author;
        
        
        $this->template = "main";        
        
        $toTpl["pagecontent"] = $this->page_model->get_page_full_content($view);  
        
        
        
        if(empty($toTpl["pagecontent"])) {
            $error['error_message'] = $this->lang->line('page-not-found');
            $toBody["content_body"] = $this->load->view('template/error', $error,true);
        }
        
        else {
            
            $this->title = $this->title.$toTpl["pagecontent"]['page_title'];
            $toTpl["title"] = $this->title;
            /* MENU */
            //$toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);

            $res = $this->getPageChildren($view, $toTpl);


            $toTpl['pageChildren'] = $res['children'];
            //$toTpl['submenu'] = $res['submenu'];

           //print_r($toTpl["pagecontent"]);
            // $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
            // $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];

            /* Carico la galleria associata alla pagina */            
            //$toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);
            
            /* Carico la galleria associata alla pagina */            
            //$toTpl['gallery_2'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_2_id']);
            
            /* Featured Projects */
            //$toTpl['projects'] = $this->projects_model->get_featured_projects();
            
            /* News per Footer */
            //$toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('awards', 5);

            /* Featured gallery */
    //        $toTpl['featured_gallery'] = $this->gallery_model->get_featured_gallery();
    //        $toTpl['featured_galler_photos'] = $this->gallery_model->get_featured_gallery_photos();

            /* Tipologia post associati a questa pagina */
            //$toTpl['posts'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

            //data
            $toBody["content_body"] = $this->load->view('pages/'.$view,array_merge($this->data,$toTpl),true);
        }
        
        $toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);
        
        
        if ($this->detect->isMobile()) {
            $toHeader["nav"] = $this->load->view("template/nav-interna",'',true);
            $toBody["header"] = $this->load->view("template/header-interna",$toHeader,true);
        }
        else {
            $toHeader["nav"] = $this->load->view("template/nav",'',true);
            $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        }

        
        $toBody["footer"] = $this->load->view("template/footer",'',true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
   /******************************
    * 
    *    TEMPLATE NEWS & PRESS
    * 
    ******************************/
    
//     protected function _render_template_news() {
        
//         $last = $this->uri->total_segments();
//         $page_name = $this->uri->segment($last);
        
//         //static
//         $toTpl["javascript"] = $this->javascript;
//         $toTpl["css"] = $this->css;
//         $toTpl["fonts"] = $this->fonts;

//         $this->template = "main";

//         //meta
//         $toTpl["author"] = $this->author;

//         $this->session->set_userdata('currentPage', current_url());

//         $toTpl["pagecontent"] = $this->page_model->get_page_full_content($page_name);

//         /* Imposto il template della pagina */
//         $template = $toTpl["pagecontent"]['template_name'];
        
//         /* MENU */
//         $toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        
//         /* Meta */
//         $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
//         $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
//         /* Carico la galleria associata alla pagina */            
//         //$toTpl["pagecontent"]['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

//         //$toTpl['big_slider'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['extra_photogallery_id']);

//         /* Titolo barra del browser */
//         $toTpl["title"] = $toTpl["pagecontent"]['page_title'];
        
//         /* Tipologia post associati a questa pagina */
//         $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('degustazioni');

//         /* Breadcrumb nella pagina */
//         //$toTpl["breadcrumb_title"] = $toTpl["pagecontent"]['title'];
        
//         /* SOTTO-MENU PAGINA */
//         $res = $this->getPageChildren($page_name, $toTpl);
//         $toTpl['children'] = $res['children'];
//         $toTpl['submenu'] = $res['submenu'];

//         //data
//         $toBody["content_body"] = $this->load->view($template,array_merge($this->data,$toTpl),true);


//         $toHeader["nav"] = $this->load->view("template/nav",'',true);
//         //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

//         $toBody["header"] = $this->load->view("template/header",$toHeader,true);
//         $toBody["footer"] = $this->load->view("template/footer",'',true);

//         $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


//         //render view
//         $this->load->view("template/skeleton",$toTpl);

//     }
    
    
//     /******************************
//     * 
//     *      ESPLOSO NEWS & PRESS
//     * 
//     ******************************/
    
//     protected function _render_news($post = '') {
        
//         $last = $this->uri->total_segments();
//         $page_url = $this->uri->segment($last);
        
//         //static
//         $toTpl["javascript"] = $this->javascript;
//         $toTpl["css"] = $this->css;
//         $toTpl["fonts"] = $this->fonts;

//         $this->template = "main";

//         //meta
//         $toTpl["author"] = $this->author;

//         $this->session->set_userdata('currentPage', current_url());

//         $toTpl["pagecontent"] = $this->post_model->get_post_by_url($page_url);

//         /* Imposto il template della pagina */
//         $template = 'dettaglio-news';
        
        
//         /* Meta */
//         $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
//         $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
//         /* Carico la galleria associata alla pagina */            
//         //$toTpl["pagecontent"]['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

//         /* Titolo barra del browser */
//         $toTpl["title"] = $toTpl["pagecontent"]['title']; 
        
//         /* Breadcrumb nella pagina */
//         //$toTpl["breadcrumb_title"] = $toTpl["pagecontent"]['title'];
        
//         /* News per Footer */
//         $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('awards', 5);
        
//         //data
//         $toBody["content_body"] = $this->load->view('pages/'.$template,array_merge($this->data,$toTpl),true);


//         $toHeader["nav"] = $this->load->view("template/nav-interna",'',true);
//         //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

//         $toBody["header"] = $this->load->view("template/header-interna",$toHeader,true);
//         $toBody["footer"] = $this->load->view("template/footer",'',true);

//         $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


//         //render view
//         $this->load->view("template/skeleton",$toTpl);

//     }
    
//     public function _render_servizi() {
        
//         $last = $this->uri->total_segments();
//         $view = $this->uri->segment($last);
        
//         //static
//         $toTpl["javascript"] = $this->javascript;
//         $toTpl["css"] = $this->css;
//         $toTpl["fonts"] = $this->fonts;

//         //meta
        
//         $toTpl["author"] = $this->author;
        
        
//         $this->template = "main";        
        
//         $toTpl["pagecontent"] = $this->page_model->get_page_full_content($view);  
        
//         if(empty($toTpl["pagecontent"])) {
//             $error['error_message'] = $this->lang->line('page-not-found');
//             $toBody["content_body"] = $this->load->view('template/error', $error,true);
//         }
        
//         else {
            
//             $this->title = $this->title.$toTpl["pagecontent"]['page_title'];
//             $toTpl["title"] = $this->title;
        
//             /* MENU */
//             //$toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);

//             $res = $this->getPageChildren($view, $toTpl);
//             $toTpl['pageChildren'] = $res['children'];
            
//             $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
//             $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];

//             /* Carico la galleria associata alla pagina */            
//             $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);
            
//             /* Featured Projects */
//             $toTpl['projects'] = $this->projects_model->get_featured_projects();
            
            
//             /* News per Footer */
//             $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('awards', 5);

//             /* Featured gallery */
//     //        $toTpl['featured_gallery'] = $this->gallery_model->get_featured_gallery();
//     //        $toTpl['featured_galler_photos'] = $this->gallery_model->get_featured_gallery_photos();

//             /* Tipologia post associati a questa pagina */
//             //$toTpl['posts'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

//             //data
//             $toBody["content_body"] = $this->load->view('pages/'.$view,array_merge($this->data,$toTpl),true);
//         }
        

//         $toHeader["nav"] = $this->load->view("template/nav-interna",'',true);
//         $toHeader['menucolor'] = "magenta";
//         $toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

//         $toBody["header"] = $this->load->view("template/header-interna",$toHeader,true);
//         $toBody["footer"] = $this->load->view("template/footer",'',true);

//         $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


//         //render view
//         $this->load->view("template/skeleton",$toTpl);
//     }

//     public function _render_lavori() {
        
//         $last = $this->uri->total_segments();
//         $view = $this->uri->segment($last);
        
//         //static
//         $toTpl["javascript"] = $this->javascript;
//         $toTpl["css"] = $this->css;
//         $toTpl["fonts"] = $this->fonts;

//         //meta
//         $toTpl["author"] = $this->author;
        
        
//         $this->template = "main";        
        
//         $toTpl["pagecontent"] = $this->page_model->get_page_full_content($view);  
        
//         if(empty($toTpl["pagecontent"])) {
//             $error['error_message'] = $this->lang->line('page-not-found');
//             $toBody["content_body"] = $this->load->view('template/error', $error,true);
//         }
        
//         else {
            
//             $this->title = $this->title.$toTpl["pagecontent"]['page_title'];
//             $toTpl["title"] = $this->title;
        
//             /* MENU */
//             //$toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);

//             $res = $this->getPageChildren($view, $toTpl);
//             $toTpl['pageChildren'] = $res['children'];
            
//             $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
//             $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];

//             /* Carico la galleria associata alla pagina */            
//             $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);
            
//             /* All Projects */
//             $toTpl['projects'] = $this->projects_model->get_all();
            
//             /* News per Footer */
//             $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('awards', 5);

//             /* Featured gallery */
//     //        $toTpl['featured_gallery'] = $this->gallery_model->get_featured_gallery();
//     //        $toTpl['featured_galler_photos'] = $this->gallery_model->get_featured_gallery_photos();

//             /* Tipologia post associati a questa pagina */
//             //$toTpl['posts'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

//             //data
//             $toBody["content_body"] = $this->load->view('pages/'.$view,array_merge($this->data,$toTpl),true);
//         }
        

//         $toHeader["nav"] = $this->load->view("template/nav-interna",'',true);
//         $toHeader['menucolor'] = "arancio";
//         $toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

//         $toBody["header"] = $this->load->view("template/header-interna",$toHeader,true);
//         $toBody["footer"] = $this->load->view("template/footer",'',true);

//         $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


//         //render view
//         $this->load->view("template/skeleton",$toTpl);
//     }    
    
    
//     public function _render_awards() {
        
//         $last = $this->uri->total_segments();
//         $view = $this->uri->segment($last);
        
//         //static
//         $toTpl["javascript"] = $this->javascript;
//         $toTpl["css"] = $this->css;
//         $toTpl["fonts"] = $this->fonts;

//         //meta
//         $toTpl["author"] = $this->author;
        
        
//         $this->template = "main";        
        
//         $toTpl["pagecontent"] = $this->page_model->get_page_full_content($view);  
        
//         if(empty($toTpl["pagecontent"])) {
//             $error['error_message'] = $this->lang->line('page-not-found');
//             $toBody["content_body"] = $this->load->view('template/error', $error,true);
//         }
        
//         else {
            
//             $this->title = $this->title.$toTpl["pagecontent"]['page_title'];
//             $toTpl["title"] = $this->title;
        
//             /* MENU */
//             //$toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);

//             $res = $this->getPageChildren($view, $toTpl);
//             $toTpl['pageChildren'] = $res['children'];
            
//             $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
//             $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];

//             /* Carico la galleria associata alla pagina */            
//             $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);
            
//             /* All Projects */
//             $toTpl['projects'] = $this->projects_model->get_all();
            
//             /* News per Footer */
//             $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('awards', 5);

//             /* Featured gallery */
//     //        $toTpl['featured_gallery'] = $this->gallery_model->get_featured_gallery();
//     //        $toTpl['featured_galler_photos'] = $this->gallery_model->get_featured_gallery_photos();

//             /* Tipologia post associati a questa pagina */
//             $toTpl['awards'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

//             //data
//             $toBody["content_body"] = $this->load->view('pages/'.$view,array_merge($this->data,$toTpl),true);
//         }
        

//         $toHeader["nav"] = $this->load->view("template/nav-interna",'',true);
//         $toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

//         $toBody["header"] = $this->load->view("template/header-interna",$toHeader,true);
//         $toBody["footer"] = $this->load->view("template/footer",'',true);

//         $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


//         //render view
//         $this->load->view("template/skeleton",$toTpl);
//     }    
    
//     public function _render_fud() {
        
//         $last = $this->uri->total_segments();
//         $view = $this->uri->segment($last);
        
//         //static
//         $toTpl["javascript"] = $this->javascript;
//         $toTpl["css"] = $this->css;
//         $toTpl["fonts"] = $this->fonts;

//         //meta
//         $toTpl["author"] = $this->author;
        
        
//         $this->template = "main";        
        
//         $toTpl["pagecontent"] = $this->page_model->get_page_full_content($view);  
        
//         if(empty($toTpl["pagecontent"])) {
//             $error['error_message'] = $this->lang->line('page-not-found');
//             $toBody["content_body"] = $this->load->view('template/error', $error,true);
//         }
        
//         else {
            
//             $this->title = $this->title.$toTpl["pagecontent"]['page_title'];
//             $toTpl["title"] = $this->title;
        
//             /* MENU */
//             //$toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);

//             $res = $this->getPageChildren($view, $toTpl);
//             $toTpl['pageChildren'] = $res['children'];
            
//             $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
//             $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];

//             /* Carico la galleria associata alla pagina */            
//             $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);
            
//             /* Carico la galleria associata alla pagina */            
//             $toTpl['gallery_2'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_2_id']);
            
            
//             /* News per Footer */
//             $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('awards', 5);

//             /* Featured gallery */
//     //        $toTpl['featured_gallery'] = $this->gallery_model->get_featured_gallery();
//     //        $toTpl['featured_galler_photos'] = $this->gallery_model->get_featured_gallery_photos();

//             /* Tipologia post associati a questa pagina */
//             $toTpl['awards'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

//             //data
//             $toBody["content_body"] = $this->load->view('pages/'.$view,array_merge($this->data,$toTpl),true);
//         }
        

//         $toHeader["nav"] = $this->load->view("template/nav-interna",'',true);
//         $toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

//         $toBody["header"] = $this->load->view("template/header-interna",$toHeader,true);
//         $toBody["footer"] = $this->load->view("template/footer",'',true);

//         $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


//         //render view
//         $this->load->view("template/skeleton",$toTpl);
//     }    
    
//     public function _render_contatti() {
        
//         $last = $this->uri->total_segments();
//         $view = $this->uri->segment($last);
        
//         //static
//         $toTpl["javascript"] = $this->javascript;
//         $toTpl["css"] = $this->css;
//         $toTpl["fonts"] = $this->fonts;

//         //meta
//         $toTpl["author"] = $this->author;
        
        
//         $this->template = "main";        
        
//         $toTpl["pagecontent"] = $this->page_model->get_page_full_content($view);  
        
//         if(empty($toTpl["pagecontent"])) {
//             $error['error_message'] = $this->lang->line('page-not-found');
//             $toBody["content_body"] = $this->load->view('template/error', $error,true);
//         }
        
//         else {
            
//             $this->title = $this->title.$toTpl["pagecontent"]['page_title'];
//             $toTpl["title"] = $this->title;
        
//             /* MENU */
//             //$toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);

//             $res = $this->getPageChildren($view, $toTpl);
//             $toTpl['pageChildren'] = $res['children'];
            
//             $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
//             $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];

//             /* Carico la galleria associata alla pagina */            
//             $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);
            
//             /* All Projects */
//             $toTpl['projects'] = $this->projects_model->get_all();
            
//             /* News per Footer */
//             $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('awards', 5);

//             /* Featured gallery */
//     //        $toTpl['featured_gallery'] = $this->gallery_model->get_featured_gallery();
//     //        $toTpl['featured_galler_photos'] = $this->gallery_model->get_featured_gallery_photos();

//             /* Tipologia post associati a questa pagina */
//             //$toTpl['posts'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

//             //data
//             $toBody["content_body"] = $this->load->view('pages/'.$view,array_merge($this->data,$toTpl),true);
//         }
        

//         $toHeader["nav"] = $this->load->view("template/nav-interna",'',true);
//         $toHeader['menucolor'] = "green";
//         $toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

//         $toBody["header"] = $this->load->view("template/header-interna",$toHeader,true);
//         $toBody["footer"] = $this->load->view("template/footer",'',true);

//         $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


//         //render view
//         $this->load->view("template/skeleton",$toTpl);
//     }        
    
    
// public function _render_lavoro() {
        
//         $last = $this->uri->total_segments();
//         $view = $this->uri->segment($last);
        
//         //static
//         $toTpl["javascript"] = $this->javascript;
//         $toTpl["css"] = $this->css;
//         $toTpl["fonts"] = $this->fonts;

//         //meta
//         $toTpl["title"] = $this->title;
//         $toTpl["author"] = $this->author;
        
        
//         $this->template = "main";        
        
//         $work_id = $this->projects_model->get_id_by_name($view);
        
//         if(empty($work_id)) {
//             show_404();
//             $error['error_message'] = $this->lang->line('page-not-found');
//             $toBody["content_body"] = $this->load->view('template/error', $error,true);
//         }
        
//         else {
        
//             /* MENU */
//             //$toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);

//             $toTpl['work_gallery'] = $this->projects_model->get_gallery_w_captions_by_item_id($work_id, $this->session->userdata('lang'));
//             $toTpl['work_detail'] = $this->projects_model->get_full_content_by_id($work_id); 
            
//             $toTpl['title'] = "Circle Studio per ".$toTpl["work_detail"]['customer'];
//             $toTpl["description"] = $toTpl["work_detail"]['meta_description'];
//             $toTpl["keywords"] = $toTpl["work_detail"]['meta_keywords'];
            
//             /* News per Footer */
//             $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('awards', 5);

//             //data
//             $toBody["content_body"] = $this->load->view('pages/work',array_merge($this->data,$toTpl),true);
//         }
        

//         $toHeader["nav"] = $this->load->view("template/nav-interna",'',true);
//         $toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

//         $toBody["header"] = $this->load->view("template/header-interna",$toHeader,true);
//         $toBody["footer"] = $this->load->view("template/footer",'',true);

//         $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


//         //render view
//         $this->load->view("template/skeleton",$toTpl);
//     }        
    
//     public function _render_pagina_testuale() {
        
//         $last = $this->uri->total_segments();
//         $page_name = $this->uri->segment($last);
        
//         //static
//         $toTpl["javascript"] = $this->javascript;
//         $toTpl["css"] = $this->css;
//         $toTpl["fonts"] = $this->fonts;

//         $this->template = "main";

//         //meta
//         $toTpl["author"] = $this->author;

//         $this->session->set_userdata('currentPage', current_url());

//         $toTpl["pagecontent"] = $this->page_model->get_page_full_content($page_name);
        
//         if(empty($toTpl["pagecontent"])) {
//             show_404();
//         }
        
       
//         /* Imposto il template della pagina */
//         $template = $toTpl["pagecontent"]['template_name'];
        
        
//         /* Meta */
//         $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
//         $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
//         /* Carico la galleria associata alla pagina */            
//         $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

//         //$toTpl['big_slider'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['extra_photogallery_id']);

//         /* Titolo barra del browser */
//         $toTpl["title"] = $this->title." - ".$toTpl["pagecontent"]['page_title'];
                

//         //data
//         $toBody["content_body"] = $this->load->view($template,array_merge($this->data,$toTpl),true);


//         $toHeader["nav"] = $this->load->view("template/nav",'',true);
//         //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

//         $toBody["header"] = $this->load->view("template/header",$toHeader,true);
//         $toBody["footer"] = $this->load->view("template/footer",'',true);

//         $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


//         //render view
//         $this->load->view("template/skeleton",$toTpl);
//     }

     
    public function getPageChildren($page_name, $toTpl) {
        $submenu = array();

        /* RECUPERO TUTTI I FIGLI DELLA PAGINA */
        $page_id = $this->page_model->get_id_by_name($page_name);
        $children_array = $this->page_model->get_page_children_of($page_id);
        $children = array();

        /* Verifico se la pagina principale va inserita nel submenu */
        if($toTpl["pagecontent"]['on_menu']) {
            array_push($submenu, $toTpl["pagecontent"]);
        }

        /* Verifico tutti i figli della pagina */
        foreach($children_array as $child) {
            $childcontent = $this->page_model->get_page_full_content($child['name']);
            if(isset($childcontent['photogallery_id'])) {
                $childcontent['gallery'] = $this->gallery_model->get_photos_by_gallery_id($childcontent['photogallery_id']);
            }
            array_push($children, $childcontent);

            /* Verifico se la pagina figlia va inserita nel menu */
            if($child['on_menu'] == 1) {
                array_push($submenu, $childcontent);
            }
        }
        return array('children' => $children, 'submenu' => $submenu);
    }

}
