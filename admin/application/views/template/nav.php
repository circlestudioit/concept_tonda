<nav>
    <img src="<?=base_url(IMAGES.'customer_logo-nav.png')?>" class="customer-logo" />
    <hr style="margin: 1em; opacity: 0.5;" />
    <p class="login-info">
        <img src="<?=base_url(IMAGES.'user.png')?>" class="user-icon" />
        <?=$this->session->userdata('first_name'); ?><br />
        <a class="white" href="/admin/admin_login/logout">Logout</a>
    </p>
    <hr style="margin: 1em; opacity: 0.5;" />
    <div class="clear"></div>
    <ul>
        <li class="<?php echo isActive($page_id,"homepage")?>"><a href="<?php echo  base_url()?>homepage">Dashboard</a></li>
        <li><hr /></li>
        <li class="<?php echo isActive($page_id,"pages_templates")?>"><a href="<?php echo base_url()?>pages_templates">Template di Pagina</a></li>
        <li class="<?php echo isActive($page_id,"pages")?>"><a href="<?php echo base_url()?>pages">Gestione pagine</a></li>
        <!--<li class="<?php echo isActive($page_id,"menu")?>"><a href="<?php echo base_url()?>menu">Menu</a></li>-->
        <li><hr /></li>
<!--        <li class="<?php echo isActive($page_id,"families")?>"><a href="<?php echo base_url()?>families">Tipologia Stanze</a></li>
        <li class="<?php echo isActive($page_id,"products")?>"><a href="<?php echo base_url()?>products">Stanze</a></li>
        <li><hr /></li>-->
        <li class="<?php echo isActive($page_id,"projects")?>"><a href="<?php echo base_url()?>projects">Gestione Progetti</a></li>
        <li><hr /></li>
        <li class="<?php echo isActive($page_id,"photogalleries_templates")?>"><a href="<?php echo base_url()?>photogalleries_templates">Template delle gallerie</a></li>
        <li class="<?php echo isActive($page_id,"photogalleries")?>"><a href="<?php echo base_url()?>photogalleries">Gallerie di immagini</a></li>
        <li><hr /></li>
        <li class="<?php echo isActive($page_id,"posts_categories")?>"><a href="<?php echo base_url()?>posts_categories">Categorie Post</a></li>
        <li class="<?php echo isActive($page_id,"posts")?>"><a href="<?php echo base_url()?>posts">Gestione Post</a></li>
        <li><hr /></li>
        <!--<li class="<?php echo isActive($page_id,"countries")?>"><a href="<?php echo base_url()?>countries">Gestione Paesi</a></li>-->
        <!--<li class="<?php echo isActive($page_id,"services")?>"><a href="<?php echo base_url()?>services">Servizi</a></li>-->
        <!--<li class="<?php echo isActive($page_id,"locations")?>"><a href="<?php echo base_url()?>locations">Locations</a></li>-->
        
    </ul>
    <footer class="footer-container">
    <div class="footer">
    &copy; <a class="white" href="http://www.circlestudio.it" target="_blank">CIRCLE STUDIO</a> 2014
    </div>
    </footer>
</nav>