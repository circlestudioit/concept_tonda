<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?php echo $this->config->item('customer-name')." - ".$title?></title>

  <?php /* codeigniter-boilerplate: Page Description**************************/ ?>
  <?php if($description):?>
  <?php /* Page description: used if the page object has a description */?>
  <meta name="description" content="<?php echo $description?>">
  <?else:?>
  <?php /* Generic site description: used if the page object has not a description */?>
  <meta name="description" content="__SITE DESCRIPTION HERE__">
  <?php endif;?>

  <meta name="viewport" content="width=device-width">
  <?php /* codeigniter-boilerplate: Styles **********************************/ ?>
  <link rel="stylesheet" href="<?php echo base_url(CSS.'normalize.css')?>">
  <?php foreach($css as $c):?>
  <link rel="stylesheet" href="<?php echo base_url(CSS.$c)?>">
  <?php endforeach;?>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="<?=base_url('resources/image_crud/js/jquery.colorbox.js')?>"></script>
  <script src="<?php echo base_url(JS.'vendor/modernizr-2.6.1.min.js')?>"></script>
  <script src="<?=base_url('resources/image_crud/js/jquery-ui.js')?>"></script>
  <script src="<?=base_url('resources/image_crud/js/fineuploader-3.2.min.js')?>"></script>

  <?php /* codeigniter-boilerplate: Google Fonts ****************************/ ?>
  <?php foreach($GFont as $f):?>
  <link  href="http://fonts.googleapis.com/css?family=<?php echo $f?>" rel="stylesheet" type="text/css" >
  <?php endforeach;?>
</head>
<body>
    <!--[if lt IE 8]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->

<div id="container">
    <header>
    <?php
    if(isset($nav)) echo $nav;
    ?>
    </header>
    <div id="main" role="main">
    <?php
    /* codeigniter-boilerplate: content from single views *********************/
    echo $content
    ?>
    </div>
  </div> <!--! end of #container -->

  
  <script>window.jQuery || document.write('<script src="<?php echo base_url(JS.'js/vendor/jquery-1.11.0.min.js')?>"><\/script>')</script>
  
  <!-- scripts concatenated and minified via ant build script-->
  <script src="<?php echo base_url(JS.'plugins.js')?>"></script>
  <script src="<?php echo base_url(JS.'main.js')?>"></script>
  <!-- end scripts-->

  <?php /* codeigniter-boilerplate: Scripts *********************************/?>
  <?php foreach($javascript as $js):?>
  <script src="<?php echo base_url()?>js/<?php echo $js?>"></script>
  <?php endforeach;?>

      <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
      <script>
          var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
          (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
          g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
          s.parentNode.insertBefore(g,s)}(document,'script'));
      </script>
      
    <!--Grocery CRUD-->
    <?php 
    if(isset($css_files)){
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
            <script src="<?php echo $file; ?>"></script>
    <?php endforeach; 
    }?>
    <!--Grocery CRUD-->

    </body>
</html>
