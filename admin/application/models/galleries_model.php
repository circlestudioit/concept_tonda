<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galleries_model extends CI_Model {


    var $table = 'photogalleries';  // Tabella madre
    var $table_photogalleries = 'photogalleries'; 
    var $table_templates = 'photogalleries_templates'; // Tabella categoria
    var $admins_table = 'admins'; // tabella degli admin
    
    
    /**
     * Author Maicol Cantagallo
     * Responsability model list of news
     * @return type 
     */
//    public function get_list() {
//        if($this->table === null)
//            return false;
//
//        $select = "{$this->table}.*";
//        $select .= ", CONCAT(".$this->admins_table.".first_name, ' ',".$this->admins_table.".last_name) as creatore";
//
//        if(!empty($this->relation))
//            foreach($this->relation as $relation){
//                list($field_name , $related_table , $related_field_title) = $relation;
//                $unique_join_name = $this->_unique_join_name($field_name);
//                $unique_field_name = $this->_unique_field_name($field_name);
//
//                if(strstr($related_field_title,'{'))
//                    $select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $unique_field_name";
//                else	  
//                    $select .= ", $unique_join_name.$related_field_title as $unique_field_name";
//
//                if($this->field_exists($related_field_title))
//                    $select .= ", {$this->table}.$related_field_title as '{$this->table}.$related_field_title'";
//            }
//
//            $this->db->select($select, false);
//            
//            $this->db->join($this->admins_table, $this->admins_table.".id = ".$this->table.".created_by");
//
//            $results = $this->db->get($this->table)->result();
//
//            return $results;
//    }
    
    public function get_template($slider_id) {
//        $this->db->select($this->table_photogalleries);
//        $this->db->join($this->table_photogalleries, $this->table_photogalleries.'.photogallery_template = '.$this->table_templates.'.id');
        $query = $this->db->get_where($this->table_photogalleries, array($this->table_photogalleries.'.photogallery_id' => $slider_id), 1, 0);
        
        return $query->row()->photogallery_template;
    }
    
    public function get_template_size($template_id) {
        $query = $this->db->get_where($this->table_templates, array($this->table_templates.'.id' => $template_id), 1, 0);
        return $query->result_array();
    }
}

?>