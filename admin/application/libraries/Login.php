<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Login Class
 *
 * Makes authentication simple
 *
 * Login is released to the public domain
 * (use it however you want to)
 * 
 * Login expects this database setup
 * (if you are not using this setup you may
 * need to do some tweaking)
 * 

	#This is for a MySQL table
	CREATE TABLE `admins` (
	`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`username` VARCHAR( 64 ) NOT NULL ,
	`password` VARCHAR( 64 ) NOT NULL ,
	UNIQUE (
	`username`
	)
	);

 * 
 */
class Login
{
	
    var $user_table = 'admins';
    
    public function loginName(){
        $CI = &get_instance();
        return $CI->session->userdata('username');
    }

    public function getUserData(){
        $CI = &get_instance();
        return $CI->session->userdata('first_name')." ".$CI->session->userdata('last_name');
    }

    public function getUserRole($user){
        $CI = &get_instance();
        $CI->db->where('username', $user);
        $query = $CI->db->get_where($this->user_table);
        return $query->result_row();
    }
    
    public function get_form_elements(){
        return array(
            'username'=>$this->username,
            'password' =>$this->password
            );
    }
    

    public function isLogged() {
        $CI =& get_instance();
        
        if($CI->session->userdata('logged_in')) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Login and sets session variables
     *
     * @access	public
     * @param	string
     * @param	string
     * @return	bool
     */
    function loginuser($user = '', $password = '') {

            //Put here for PHP 4 users
            $CI = &get_instance();

            //Make sure login info was sent
            if($user == '' OR $password == '') {
                    return false;
            }

            //Check if already logged in
            if($CI->session->userdata('username') == $user) {
                //User is already logged in.
                return false;
            }

            //Check against user table
            $CI->db->where('username', $user); 
            $query = $CI->db->get_where($this->user_table);

            if ($query->num_rows() > 0) {

                    $row = $query->row_array(); 

                    //Check against password
                    if(md5($password) != $row['password']) {
                            return false;
                    }
                    else {

                        //Destroy old session
                        $CI->session->sess_destroy();

                        //Create a fresh, brand new session
                        $CI->session->sess_create();

                        //Remove the password field
                        unset($row['password']);

                        $data = array('logged_in' => TRUE, 'lang' => strtoupper($row['language']),
                                      'first_name' => $row['first_name'], 'last_name' => $row['last_name'], 'username' => $row['username'],
                                      'user_role' => $row['role'], 'active_langs' => $CI->config->item('active_langs'));
                        //Set session data
                        $CI->session->set_userdata($data);

                        //Login was successful
                        return true;
                    }
            } else {
                    //No database result found

                    return false;
            }	

    }

    /**
     * Logout user
     *
     * @access	public
     * @return	void
     */
    function logout() {
        $CI = &get_instance();		

        //Destroy session
        $CI->session->sess_destroy();
        return true;
    }

    function create($user = '', $password = '', $auto_login = true) {
            //Put here for PHP 4 users
            $CI =& get_instance();

            //Make sure account info was sent
            if($user == '' || $password == '') {
                    return false;

            }

            //Check against user table
            $CI->db->where('username', $user);
            $query = $CI->db->get($this->user_table);
            if ($query->num_rows() > 0) {
                    //username already exists
                    return false;

            } else {

                    //Encrypt password
                    $password = md5($password);

                    //Insert account into the database
                    $data = array(
                                            'username' => $user,
                                            'password' => $password
                                    );
                    $CI->db->set($data);
                    if(!$CI->db->insert($this->user_table)) {
                            //There was a problem!
                            return false;
                    }
                    $user_id = $CI->db->insert_id();

                    //Automatically login to created account
                    if($auto_login) {
                            //Destroy old session
                            $CI->session->sess_destroy();

                            //Create a fresh, brand new session
                            $CI->session->sess_create();

                            //Set session data and set logged_in to true
                            $CI->session->set_userdata(array('id' => $user_id,'username' => $user, 'logged_in' => true));

                    }

                    //Login was successful
                    return true;
            }

    }

    /**
     * Delete user
     *
     * @access	public
     * @param integer
     * @return	bool
     */
    function delete($user_id) {
            //Put here for PHP 4 users
            $CI = &get_instance();

            if(!is_numeric($user_id)) {
                    //There was a problem
                    return false;
            }

            if($CI->db->delete($this->user_table, array('id' => $user_id))) {
                    //Database call was successful, user is deleted
                    return true;
            } else {
                    //There was a problem
                    return false;
            }
    }


}
?>