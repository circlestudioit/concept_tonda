<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_login extends BP_Controller {

    
    var $data;
    
    public function __construct() {
            parent::__construct();
            $this->load->library('table');
            $this->load->helper('form');
        }
        
    public function index($error = '')
    {
        $this->hasNav = false;
        $this->title = "Login";
        $this->css = array("style.css");
        
        if(!$this->login->isLogged()) {
            $toView['content'] = $this->load->view('pages/admin_login', $this->data, true);
        }
        
        if($error != '') {
            $toView['error'] = $error;
        }

        $this->build_content($toView);
        $this->render_page();
    }
    
    public function login() {
        $this->load->library('form_validation');
//        $this->load->library('email');
        
        // field name, error message, validation rules
        $this->form_validation->set_rules('login_username', 'Nome utente', 'trim|required|min_length[4]|xss_clean');
        $this->form_validation->set_rules('login_password', 'Password', 'trim|required|min_length[4]|max_length[32]');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        if($this->form_validation->run() == FALSE) {
           $this->index();
        }

        else if($this->form_validation->run() == TRUE) {
            if($this->login->loginuser($this->input->post('login_username'), $this->input->post('login_password'))) {
            redirect('/homepage');
        }
            else
                $this->index("C'è stato un problema con il login. Prova di nuovo");
        }
    
    }
    
    public function logout() {
        if($this->login->logout()) {
            redirect('/');
        }
//        else {
//            $this->error();
//        }
    }
    
    function error(){
            $this->load->view('pages/logerror_view',$this->data);
    }
}
/*End of file example.php*/
/*Location .application/controllers/example.php*/