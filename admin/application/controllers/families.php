<?php

class Families extends BP_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD'); // istanza crud
        $this->load->library('image_CRUD');
        
        $this->active_langs = $this->config->item('active_langs');
        $this->subject              = 'Famiglie';        

        $this->module                   = 'families';
        $this->table                    = 'products_families';
        $this->table_i18n               = 'products_families_i18n';
        
//        $this->products_categories      = 'products_categories';                // categorie di prodotto (all'uovo, casereccia, piccola, di una volta, semola fiore, preziosa, tradizionale, a matassa)
//        $this->products_categories_i18n = 'products_categories_i18n';           // categorie di prodotto (all'uovo, casereccia, piccola, di una volta, semola fiore, preziosa, tradizionale, a matassa)
        
//        $this->relation_table           = "prodcategories_and_families";            // relazione n-n tra categorie e famiglie
        $this->module_galleries         = 'photogalleries';
    }

    
    /**
     * Author: Raffaele Rotondo
     * Responsability: attraverso il CRUD vengono stabiliti i dati da 
     * visualizzare in fase di view, edit ed insert
     * @param type $output 
     */
    function index() {
        $this->css = array("admin.css");
        
        try {
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');
            $crud->set_table($this->table);
            $crud->set_subject('Tipologie di stanza');
            $crud->unset_print();
            $crud->unset_export();
            
            //$crud->set_relation('family_id', $this->products_categories, 'name');
//            $crud->set_relation_n_n('categories', $this->relation_table, $this->products_categories, 'product_id', 'category_id', 'name','priority');
//            $crud->set_relation_n_n('also_in', $this->relation_table, $this->products_categories, 'product_id', 'category_id', 'name', 'priority');
            
            $crud->set_field_upload('thumb', $this->config->item('products_thumb'));                
            $crud->set_field_upload('image', $this->config->item('products_image'));
			
   
            $crud->display_as('family_name','Nome');
            $crud->display_as('is_published','Pubblicato');
            //$crud->display_as('is_public','Linea da collezione');
            $crud->display_as('created_by','Creato da');
            $crud->display_as('thumb','Thumb');
            $crud->display_as('image','Immagine');
            $crud->display_as('ord','Ordine');
            //$crud->display_as('color','Colore');
            
            /* Aggiungo la bandierina per editare ogni lingua attiva */
            $array_language = $this->active_langs;
            for($i=0; $i<count($array_language); $i++) {
                $crud->add_action('Language'.$array_language[$i], base_url(IMAGES.$array_language[$i].'_flag.png'), base_url($this->module.'/language/'.$array_language[$i].'/edit').'/');
            }
            
            //Campi visualizzati quando aggiungo un elemento
            $crud->fields('family_name', 'thumb', 'image', 'ord', 'is_published');       
            
            //Colonne visualizzate nella grid dell'elenco di tutti i prodotti
            $crud->columns('family_name', 'is_published', 'ord');
            
            $output = $crud->render();
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();

        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
    /**
     * Author: Maicol Cantagallo
     * Responsability: gestisce la modifica della lingua di un post 
     */
    function language($_lang) {
        $this->css = array("admin.css");
       
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        
        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');
            $crud->set_table($this->table_i18n);
            $crud->set_subject('lingua '.$_lang);
            
            $crud->unset_print();
            $crud->unset_export();
            $crud->unset_texteditor('description');
            $crud->unset_texteditor('content');
            
            $crud->display_as('title','Title');                                   
            $crud->display_as('headline','Headline');
            $crud->display_as('content','Content');
            $crud->display_as('keywords','Keywords (SEO)');
            $crud->display_as('description','Description (SEO)');
            
            $crud->fields('title', 'headline', 'content', 'keywords', 'description');
            
            /* Imposto la primary key con id e lingua, per poter editare la lingua giusta */
            $crud->set_primary_key(array('id' => $record_num,'lang' => $_lang));
            
            /* Sostituisco la update originale con quella personalizzata per l'inserimento di una nuova riga */
            $crud->callback_update(array($this,'update_this_language'));

            $output = $crud->render();
            
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();

        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
    function update_this_language($post_array, $primary_key) {
        
        /* Recupero la lingua dall'indirizzo */
        $last = $this->uri->total_segments();
        $lang = $this->uri->segment($last-2);
        
        try {
            /* Controllo se la lingua è già esistente nel db */
            $query = $this->db->get_where($this->table_i18n, array('id' => $primary_key, 'lang' => $lang));
            
            /* Se c'è già devo solo aggiornarla */
            if($query->num_rows() > 0) {
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
                return true;
            }
            /* Altrimenti devo inserire una nuova riga con quella lingua, e poi aggiornarla con i dati del form */
            else {
                $data = array('id' => $primary_key, 'lang' => $lang);
                $this->db->insert($this->table_i18n, $data);
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
            }
            
        } catch (Exception $ex) {
            return $ex;
        }

    }
    

}

?>